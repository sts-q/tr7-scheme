; =============================================================================
; :file: lib-test.scm           ; tests for lib.scm
; flat flatten ;

(define (__lib) ; and FOREIGNS
   ; (test '+             (+ 3 4)                                 8)

; :chapter: MATH
   (test '+             (+ 3 4)                                 7)

   (test 'fac           (fac 10)                                3628800)
   (test 'fib           (fib 10)                                55)

; :chapter: STRINGS
   (test 'string-blank? (string-blank? "")                      #t)
   (test 'string-blank? (string-blank? " ")                     #t)
   (test 'string-blank? (string-blank? "  ")                    #t)
   (test 'string-blank? (string-blank? "  a  ")                 #f)

   (test 'string-head?  (string-head? "otto" "ot")              #t)
   (test 'string-head?  (string-head? "otto" "oT")              #f)

   (test 'string-maximal (string-maximal 4 "This is a string.") "This")
   (test 'string-maximal (string-maximal 4 "Th")                "Th")
   (test 'string-maximal (string-maximal 4 "")                  "")

   (test 'chunks        (chunks "This is a string.")            '("This" "is" "a" "string."))
   (test 'chunks        (chunks "This   is   a    string.")     '("This" "is" "a" "string."))
   (test 'chunks        (chunks " This is a string. ")          '("This" "is" "a" "string."))
   (test 'chunks        (chunks "   This is a string.   ")      '("This" "is" "a" "string."))
   (test 'chunks        (chunks "a")   '("a"))
   (test 'chunks        (chunks "")    '())

   (test 'chunks-1      (chunks-1 #\; "")                       '(""))
   (test 'chunks-1      (chunks-1 #\; ";")                      '("" ""))
   (test 'chunks-1      (chunks-1 #\; ";;")                     '("" "" ""))
   (test 'chunks-1      (chunks-1 #\; ";otto;")                 '("" "otto" ""))
   (test 'chunks-1      (chunks-1 #\; "sarah;otto;levito")      '("sarah" "otto" "levito"))


; :chapter: LISTS
   (test 'find          (find odd? '(2 4 6 1 2 3 4))            '(1 2 3 4))
   (test 'find          (find odd? '(1 2 3 4))                  '(1 2 3 4))
   (test 'find          (find odd? '(2 4 6))                    '())
   (test 'findpos       (findpos odd? '(2 4 6 1 2 3 4))         3)
   (test 'findpos       (findpos odd? '(1 2 3 4))               0)
   (test 'findpos       (findpos odd? '(2 4 6))                 #false)
   (test 'findpos       (findpos (partial < 3) '(0 1 2 3 4 5))  4)

   (test 'flatten       (flatten '((1 2 3)))                    '(1 2 3))
   (test 'flatten       (flatten '((1 2 3)(4 5 6)(7 8 9)))      '(1 2 3 4 5 6 7 8 9))
   (test 'flatten       (flatten '((1 (11 22 33) 3)(4 5 6)))    '(1 (11 22 33) 3 4 5 6))
;ERR: (test 'flatten    (flatten '((1 2 3) 11 (4 5 6)))         '(..))

   (test 'flat          (flat )                                 '())
   (test 'flat          (flat 1 2 3)                            '(1 2 3))
   (test 'flat          (flat 1 '(11 22 33) 3)                  '(1 11 22 33 3))
   (test 'flat          (flat 1 '(a b) 2 '(a (aa(bbb ccc))) 3)  '(1 a b 2 a aa bbb ccc 3))


   (test 'range-0       (range-0 10)                            '(0 1 2 3 4 5 6 7 8 9))
   (test 'range-1       (range-1 10)                            '(  1 2 3 4 5 6 7 8 9 10))

   (test 'pick          (pick '(1 3 3 5) '(0 11 22 33 44 55 66)) '(11 33 33 55))

   (test 'remove        (remove even? '(0 1 2 3 4 5 6 7 8 9))   '(1 3 5 7 9))

   (test 'slice         (slice even? odd? '(1 3  4 6  5 7))     '(4 6))
   (test 'slice         (slice even? odd? '(1 3))               '())
   (test 'slice         (slice even? odd? '(4 6))               '(4 6))
   (test 'slice         (slice even? odd? '())                  '())
   (test 'slice         (slice id id '(#f #f #f 1 2 3 #f #f #f)) '())
   (test 'slice         (slice id (complement id) '(#f #f 1 2 #f #f)) '(1 2))

   (test 'sort          (sort '( 9 4 3 8 7 4 5 ))               '(3 4 4 5 7 8 9))
   (test 'sort          (sort '(otto maxi henry abel))          '(abel henry maxi otto))
   (test 'sort          (sort '(maxi 4 able 9 3(1 2 3)(1 2 2)"moo"))'(3 4 9 "moo" able maxi (1 2 2)(1 2 3)))
   (test 'sort          (sort '(#\a "a"))                       '(#\a "a"))

   (test 'sort-with     (sort-with first  '((1 2)(4 3)(5 6)(3 9)))   '((1 2)(3 9)(4 3)(5 6 )))
   (test 'sort-with     (sort-with second '((1 2)(4 3)(5 6)(3 9)))   '((1 2)(4 3)(5 6)(3 9)))
   (test 'sort-with     (sort-with sum    '((1 2)(4 3)(5 6)(3 9)))   '((1 2)(4 3)(5 6)(3 9)))

   (test 'separate      (let-values (((ts fs) (separate even? '(1 2 3 4 5 6 7 8 9 0)))) (list ts fs))
                                                                (list '(2 4 6 8 0) '(1 3 5 7 9)))

; :chapter: COMPARE
   (test 'lesser?       (lesser? 1 2)                           #t)
   (test 'lesser?       (lesser? 2 2)                           #f)
   (test 'lesser?       (lesser? 3 2)                           #f)
   (test 'lesser?       (lesser? 'a 'b)                         #t)
   (test 'lesser?       (lesser? 'b 'b)                         #f)
   (test 'lesser?       (lesser? 'c 'b)                         #f)
   (test 'lesser?       (lesser? "a" "b")                       #t)
   (test 'lesser?       (lesser? "b" "b")                       #f)
   (test 'lesser?       (lesser? "c" "b")                       #f)
   (test 'lesser?       (lesser? #f #t)                         #t)
   (test 'lesser?       (lesser? #t #t)                         #f)
   (test 'lesser?       (lesser? #f #f)                         #f)
   (test 'lesser?       (lesser? '() '())                       #f)
   (test 'lesser?       (lesser? '() '(1))                      #t)
   (test 'lesser?       (lesser? '(1) '())                      #f)
   (test 'lesser?       (lesser? '(1 2) '(1 2))                 #f)
   (test 'lesser?       (lesser? '(1 2 (1 2)) '(1 2 (1 2)))     #f)
   (test 'lesser?       (lesser? '(1 2 (1 2)) '(1 2 (1 1)))     #f)
   (test 'lesser?       (lesser? '(1 2 (1 2)) '(1 2 (1 3)))     #t)
   (test 'lesser?       (lesser? #t 1)                          #t)
   (test 'lesser?       (lesser? #t 'otto)                      #t)
   (test 'lesser?       (lesser? '(1 2 3) "1 2 3")              #f)

; :chapter: SHOW
   (test 'show-bin      (show-bin 0 #b_110011)                  "110011")
   (test 'show-hex      (show-hex 0 #x_FFFF)                    "ffff")
   (test 'show-oct      (show-oct 0 #o_644)                     "644")


; :chapter: SCAN
   (test 'scan-cents    (scan-cents "123,89")                   12389)
   (test 'scan-cents    (scan-cents "  123,89")                 12389)
   (test 'scan-cents    (scan-cents "-123,89")                 -12389)
   (test 'scan-cents    (scan-cents "0,00")                         0)
   (test 'scan-cents    (scan-cents "-0,00")                        0)
   (test 'scan-cents    (scan-cents "123,89  ")                    #f)
   (test 'scan-cents    (scan-cents "123.89")                      #f)
   (test 'scan-cents    (scan-cents "0,0")                         #f)
   (test 'scan-cents    (scan-cents "0,3")                         #f)
   (test 'scan-cents    (scan-cents "0,000")                       #f)

; :chapter: COMBINATORS
   (test 'id            (id 3)                                      3)
   (test 'complement    ((complement even?) 3)                     #t)
   (test 'complement    ((complement odd?)  3)                     #f)
   (test 'complement    (filter (complement even?) '(0 1 2 3 4))   '(1 3))

   (test 'compose       ((compose) 1 2 3)                          1)
   (test 'compose       ((compose 1+) 1)                           2)
   (test 'compose       ((compose 1+ (partial + 10)) 70 20 10)     111)
   (test 'compose       ((compose 1+ (partial * 2) (partial + 10)) 70 20 10) 221)
   
   (test 'partial       ((partial + 1 2 3) 4 5 6)                  21)
   (test 'partial       ((partial < 3) 4)                          #t)
   (test 'partial       ((partial <) 3 4)                          #t)
   (test 'partial       ((partial < 3 4) )                         #t)

   (test 'pipe/v        ((pipe + (partial + 10) (partial * 2) 1+ 1+) 70 30)    222)

   (test 'pipe          ((pipe)                                        70 20 10)    70)
   (test 'pipe          ((pipe +)                                      70 20 10)   100)
   (test 'pipe          ((pipe + (partial + 10))                       70 20 10)   110)
   (test 'pipe          ((pipe + (partial + 10) 1+)                    70 20 10)   111)
   (test 'pipe          ((pipe + (partial + 10) (partial * 2) 1+)      70 20 10)   221)
   (test 'pipe          ((pipe + (partial + 10) (partial * 2) 1+ 1+)   70 20 10)   222)
;  (test 'pipe          ((pipe + (partial + 10) (partial * 2) 1+ 1+ 1+)70 20 10)   222)  ; pipe max 5 funs

; :chapter: foreigns
   (test 'bit-shl       (bit-shl 1 1)                           2)
   (test 'bit-shl       (bit-shl 4 4)                          64)
   (test 'bit-shl       (bit-shl -1 -16)                   -65536)
   (test 'bit-asr       (bit-asr 2 1)                           1)
   (test 'bit-asr       (bit-asr 64 4)                          4)
   (test 'bit-and       (bit-and)                              -1)
   (test 'bit-and       (bit-and 1)                             1)
   (test 'bit-and       (bit-and 1 3 5 9)                       1)
   (test 'bit-or        (bit-or)                                0)
   (test 'bit-or        (bit-or 1)                              1)
   (test 'bit-or        (bit-or 1 2 4 8)                       15)


)

(define (__alist/v)
  (define al (alist-new))
  (section "__alist")
  (set! al (alist-new))
  (set! al (alist-cons al 1 11))
  (set! al (alist-cons al 2 22))
  (set! al (alist-cons al 3 33))
  (alist-print 12 al)
  (lf)
  (print  (alist-has-key? al 2))
  (print  (alist-has-key? al 200))
  (lf)
  (set! al (alist-remove al 200))
  (set! al (alist-remove al 2))
  (set! al (alist-remove al 2))
  (set! al (alist-update al 3 333))
  (set! al (alist-update al 4 4444))
  (alist-update! al 5 55)
  (alist-update! al 5 5500)
  (print (alist-exchange! al 8 88)) (spc)
  (print (alist-exchange! al 8 880088)) (spc)
  (print (alist-exchange! al 8 881188)) (spc)
  (alist-print 12 al)
  (lff)
  )

(define (__files/v)
   (section "__files")
   (alist-print 64 file-touch-times)
   )

; =============================================================================
; :file: END sts-q 2023-Sep

