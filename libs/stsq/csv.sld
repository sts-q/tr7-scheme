; =============================================================================
; :file: csv.sld               ; CSV

; =============================================================================
(define-library (stsq csv)
   (export
      csv-strings)

   (import
      (scheme base)
      (stsq prelib)
      (stsq lib))

  (include "csv.scm")
  (include "csv-test.scm")

  (begin
     (__csv)
     ))

   
; =============================================================================
; :file: END sts-q 2023-Sep

