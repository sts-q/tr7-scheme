; =============================================================================
; :file: random.scm             ; Random numbers.

; =============================================================================
; :chapter: RANDOM
(define random-seed (exact (round (current-second))))

(define (random-next)
  (let* ((a 16807)
         (b 2147483647))
    (set! random-seed (mod (* a random-seed) b))
    random-seed))

(define (random-roll n) (mod (random-next) n))          ; #DOC Return random interger: 0..(n-1)
(define (random-bool) (odd? (random-next)))             ; #DOC Return random boolean.

(define (random-pop l)                                  ; #DOC Return random elt of l and new l without elt.
  (assert (not (null? l)) "random-pop: l is empty.")
  (let* ((len (length l))
         (i (random-roll len))
         (x (list-ref l i)))
    (values x (append (take i l) (drop (+ 1 i) l)))))

(define-syntax random-pop!                              ; #DOC Return random elt and update l!
  (syntax-rules ()
    ((me lst)
     (let-values (((x l) (random-pop lst)))
       (set! lst l)
       x))))

(define (random-shuffle l)                              ; #DOC Return l shuffled.
  (define (loop l nl)
    (cond
      ((null? l) nl)
      (else (let-values (((x l) (random-pop l))) (loop l (cons x nl))))))
  (loop l '()))

(define (random-parts-per n m) (< (random-roll m) n))   ; #DOC Return boolean, that is n-of-m times true.



; =============================================================================
(print! 'random/)
; :file: END sts-q 2023-Sep
