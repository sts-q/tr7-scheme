;;; ===========================================================================
;;; :file: pub.scm  --  Get html with help of xmlgen.

;;; ===========================================================================
(define (xtitle mess)           (xtag 'title '()  (list mess)))                 ; #DOC
(define (xmeta name value)      (xtag 'meta  (list (xatt name value)) #f))      ; #DOC
(define (xstyle text)           (xtag 'style '() (list (show-text text))))      ; #DOC

(define (xhtml . ts)            (xtag 'html '() ts))                            ; #DOC
(define (xhead . ts)            (xtag 'head '() ts))                            ; #DOC
(define (xbody . ts)            (xtag 'body '()
                                   (list (xtag 'div (list (xatt 'id 'contents)) ts))))

(define (xh1 mess)              (xtag 'h1   '() (list mess)))                   ; #DOC
(define (xh2 mess)              (xtag 'h2   '() (list mess)))                   ; #DOC

(define (xp . content)          (xtag 'p    '() content))                       ; #DOC
(define (xa text link)          (xtag 'a  (list (xatt 'href link)) (list text))); #DOC
(define (xbr)                   (xtag 'br '() #f))                              ; #DOC


(define (xheadline mess)        (xtag 'div (list (xatt 'id 'head))              ; #DOC
                                   (list (xtag 'h1 '() (list mess)))))

(define (xchapter mess . ts)    (xtag 'div (list (xatt 'id 'body))              ; #DOC
                                   (cons (xtag 'h3 '() (list mess))
                                         ts)))

(define (xcode text)            (xtag 'pre '() (list "\n" (show-text text))))

(define (xfoot . ts)            (xtag 'div (list (xatt 'id 'foot))              ; #DOC
                                   (list (xtag 'h3 '() ts))))


(define (xprint t)              (print (xtag-show t)))                          ; #DOC


;;; ===========================================================================
;;; :file: END sts-q 2024-Aug
