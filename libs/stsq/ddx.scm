; =============================================================================
; :file: ddx.scm  --  Dot Documentation.

; shell say scmdata ;
; =============================================================================
; :chapter: LIB

(define dir-libs (path-pwd "libs/stsq/"))
(define dir-scm (path-pwd "scm/"))
(define cmd-grep "grep -h ")                                                  ; -h: suppress prefix of filename
(define (patt-define patt) (string-append "'^\(define .*" patt))                ; )


; =============================================================================
; :chapter: MAIN
(define-syntax dda  (syntax-rules () ((_ name) (do-dda 'name))))                ; #DOC #DDX Libraray API.
(define-syntax ddw  (syntax-rules () ((ddw x)  (do-ddw 'x))))                   ; #DOC #DDX Doc of word
(define-syntax ddws (syntax-rules () ((ddws x) (do-ddws 'x))))                ; #DOC #DDX Find words with <seq>
(define-syntax ddg  (syntax-rules () ((ddg  x) (do-ddg  'x))))           ; #DOC #DDX Grep tr7.c for _PROC_ word

(define (do-dda name)
  (let* ((n     (cond
                   ((symbol? name)      (symbol->string name))
                   ((string? name)      name)
                   ((number? name)      (number->string name))
                   (else                #f)))
         (path  (cond
                   ((not n)             #f)
                   (else
                      (let ((p-libs  (path-pwd "/libs/stsq/" n ".scm"))
                            (p-scm   (path-pwd "/scm/"       n ".scm")))
                         (cond
                            ((file-exists? p-libs)      p-libs)
                            ((file-exists? p-scm)       p-scm)
                            (else                       #f)
                            ))))))
    (ifnot path
           (say / "No such library found.")
           (system (cmd "egrep '\\(define.*#DOC|-------|=======|:file:' " path))                ; )
           )
    ok))

(define (do-ddw word)
  (let* ((path (path-pwd "libs/stsq/*.scm"))
         (patt (string-append "'\\(define(-syntax | \\()?"   (symbol->string word)   ".*#DOC'")) ;)
         (grep "egrep -h ")
         (cmd (string-append grep patt "  " path)))
;     (lf) (print cmd)
    (say / !!)
    (system cmd)
    (lf))
  (let* ((all (flatten (map fourth *doctab*))))
     (for-each
           (lambda (x) (say / CMT "*doctab*:   ") (println x))
           (filter   (lambda (x) (and (pair? x) (pair? (car x)) (eqv? (caar x) word)))   all)))
  (let* ((file (path-pwd "libs/stsq/" "lib-test" ".scm"))
         (patt (string-append "'test.*" (symbol->string word) "'"))
         (c    (string-append "egrep " patt "  " file))
         )
    (lff) (msleep 250)
    (system c))
  ok)

(define (do-ddws patt)
  (let* ((syms (symbols-set))
         (len  (length syms))
         (seq  (if (symbol? patt) (symbol->string patt) patt))
         (count 0)
         (match? (lambda (s) (string-subseq? (symbol->string s) seq)))
         (out (lambda (s)
                 (print "   ")
                 (print (if (defined? s) "def  " "     "))
                 (print s)
                 (lf)
                 (incr! count))))
    (say /) (for-each out (sort (filter match? syms)))
    (say / CMT _ count "/" len "symbols found." //)
    ok
    ))

(define (ddls)                                                     ; #DOC #DDX List all libraries from *doctab*
  (section "libs  *doctab* knows about:")
  (letrec ((ls  (sort (map (partial take 3) *doctab*)))
           (out (lambda (l)
                  (lf)     (print (first l))
                  (tab 25) (print (second l))
                  (tab 40) (print (third l)))))
    (for-each out ls)
    (lf) ok))

(define-syntax ddt                                                 ; #DOC #DDX Find all words taged with <word>
  (syntax-rules ()
    ((_ word)
     (begin
       (let* ((cmd "grep -h '#")
              (patt (string-upcase (symbol->string 'word)))
              (path (path-pwd ""))
              (libs (string-append cmd patt "' " path "/libs/stsq/*scm"))
              (scm (string-append cmd patt "' " path "/scm/*.scm")))
         #(begin (println libs) (println scm))
         (say / !!)
         (system libs)
         (system scm))))))

(define (ddts)                                                                  ; #DOC #DDX List all found tags
  (let* ((tempfile      (path-pwd "work/tempfile.txt"))
         (libs          (path-pwd "libs/stsq/*"))
         (scm           (path-pwd "scm/*"))
         (patt          "'define.*;.*#'")
         (no '          ("#false." "#true." "#f" "#t" "#f." "#t." "#\\m))"))              ; ((
         (grep-1        (cmd "grep -h" patt libs ">"  tempfile))
         (grep-2        (cmd "grep -h" patt  scm ">>" tempfile))
         (tags          '()))

    #(begin (lf) (print tempfile)
            (lf) (print libs)
            (lf) (print scm)
            (lf) (print grep-1)
            (lf) (print grep-2))

    (system grep-1)
    (system grep-2)
    (letrec ((lines   (file-read-lines tempfile))
             (tags    '())
             (collect (lambda (line)
                        (let* ((rest (string-tail line (string-find-char line (integer->char 59) 0)))  ; ';'
                               (cs (chunks rest))
                               (ts (filter (lambda (wrd) (char-at=? wrd 0 #\#)) cs)))
                          (for-each (lambda (t) (push! t tags)) ts))))
             (loop (lambda (lines)
                     (cond
                       ((null? lines) #t)
                       (else (collect (car lines)) (loop (cdr lines)))))))
      (loop lines)
      (tt-print 12 'tags (transpose (list->table 4 (sort (remove (partial (flip member) no) (nub tags)))))))))

(define (do-ddg word)
   (let* ((w   (cond
                 ((string? word)  word)
                 ((symbol? word)  (symbol->string word))
                 (else            (err "string expected: " word))
                 ))
          (p   "_PROC_.*")
          (c   (string-append "grep -ni " "'"p w"'  " (path-pwd "c/" "tr7.c")))

         )
      (say / !!)
      (system c)
   ))

(define-syntax trans                   ; #DOC #DDX translate de-en offline with local dictionary of tu-chemnitz
  (syntax-rules ()
    ((_ word)   (do-trans 'word #f))
    ((_ word p) (do-trans 'word 'p))
    ))

(define (do-trans word p)
  (let* ((++ string-append)
         (tempfile (path-pwd "work/tempfile.txt"))
         (dictfile (path-home "adds/dict-tu-chemnitz/de-en.txt"))
         (patt (further word symbol? symbol->string))
         (out (lambda (line)           ; dict-entry-line: ..de.. :: ..en..   (separator: ::)
                (let ((cs (chunks-1 #\: line)))
                  (lff) (print (first cs))
                  (lf) (print (third cs)))))
         (cmd (cond
                ((eqv? p '^)
                 (++ "egrep -w '^" patt "' " dictfile))
                (p
                 (++ "egrep -w  '" patt "' " dictfile))
                (else
                 (++ "egrep     '" patt "' " dictfile))))
         (:: (system (++ cmd " >" tempfile)))
         (lines (file-read-lines tempfile)))
    (section word)
    #(begin (lf) (print cmd))
    (for-each out lines)
    (lff)))


(define (help)                                                                  ; #DOC #DDX Print short help
  (dot-as-section)
  (say "============  ddx help: dd like Dot Documentation: dots on screen or on paper as if printed.")
  (dot-as-text)
  (say "\n    $ ./seven                  Print help for seven.")
  (say "\n    ?                          Print help for scm-repl.")
  (say "\n    (say?)                     Print help for 'say'")
  (say "\n    (dda name)                 Print library exports.             e.g.: (dda random)")
  (say "\n    (ddw word)                 Print definition of word.          e.g.: (ddw fib)")
  (say "\n    (ddt tag)                  Print words tagged with <tag>.     e.g.: (ddt alist)")
  (say "\n    (ddws sym)                 Print words containing sequence 'sym'.   (ddws show)")
  (say "\n    (ddws \"string\")            Print words containing 'string'.")
  (say "\n    (ddts)                     List all seen tags.")
  (say "\n    (ddls)                     Print list of libraries.")
  (say "\n    (ddg <string>)             Grep _PROC__.*<string>  in tr7.c   e.g.: (ddt \"with\")" )
  (say "\n    (trans word)               Translate word de - en offline. Requires dictionary file.")
  (say "\n    (system \"<shell-cmd>\")   Run shell command.                 e.g.: (system \"ls -lA\")")
  (lff)
  (say "Assumed window width through all of this repo is 112 characters.      ..................                   --> #")
  (lff))

(define (hello!)   (lf) (print "Seven for TR7 ") (print (tr7-version)) (lf))    ; #DOC #DDX Print short greeting

(define (hello?)                                                                ; #DOC #DDX Print long greeting
   (clr) (say // //)
   (hello!)
   (system "./seven -v"))


; =============================================================================
; :chapter: TEST
(define (__ddx/v)
   (say /=== '__ddx/v ===/)
   (goes 'from-ddx
      (dda random)
      (ddw take)
      (ddt math)
      (ddg "current")
      (ddws add)
      (ddls)
      (ddts)
      ))


; =============================================================================
(print! 'ddx/)
; :file: END sts-q 2023-Oct

