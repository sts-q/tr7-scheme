;;; ===========================================================================
;;; :file: xmlgen-test.scm

(define (__xatt)
   (let* ((a    (xatt 'otto "quite-often"))
          (b    (xatt 'frank 'as-always))
          (c    (xatt 'julie 12000))
          (s1   "This is a <string>.")
          (s2   "A next line of 'text'")
          (s3   "next-string")
          (h1   (xtag 'h1 (list a b c)  (list s1 s1 s1 s1)))
          (h2   (xtag 'h2 (list (xatt 'color "brown") (xatt 'bg "yellow"))
                            (list s1 s2 s3" FIN ")))
          (br   (xtag 'br (list (xatt 'color "brown") (xatt 'bg "yellow")) #f))

;           (err1 (xatt 'morten 12.2))
;           (err2 (xtag 'err '() (list 12.3)))

        )
     (lf) (print "-------  xatt  -------")
     (lf) (print a)
     (lf) (print b)
     (lf) (print c)
     (lf) (print (xatt-show a))
     (lf) (print (map xatt-show (list a b c)))
     (lf) (print (xatts-show (list a b c)))
     (lff)
     (lf) (print "-------  xtag  -------")
     (lf) (print h1)
     (lf) (print h2)
     (lf) (print (xtag-show h1))
     (lf) (print (xtag-show h2))
     (lf) (print (xtag-show br))
     (lf)
     (lff)(print "-------  translate string")
     (lf) (print (xtag-show "translate string:  <>&\"' <>&\"' :string"))
     (lff)
;      (lf) (print (xtag-show err))
     )
   )

(define (__xmlgen/v)
   (say /=== '__xmlgen/v )
   (__xatt)
   (say /=== "__xmlgen/v done" //)
   (dot-as-std))


;;; ===========================================================================
;;; :file: END sts-q 2024-Aug
