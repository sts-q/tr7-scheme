;;; ============================================================================
;;; :file: tt.sld              Simple table functions.

;;; ===========================================================================
(define-library (stsq tt)
  (export
    rec-addcol
    rec-has-at?
    tt-only
    tt-colsum
    tt-map!
    tt-map-col
    tt-print
    __tt/v)

  (import
    (scheme base)
    (stsq prelib)
    (stsq lib))

  (include "tt.scm")
  (include "tt-test.scm")

  )


;;; ===========================================================================
;;; :file: END sts-q 2023-Nov

