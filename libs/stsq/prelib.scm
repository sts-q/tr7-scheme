;;; ===========================================================================
;;; :file: prelib.scm  --  Basic stuff, always loaded: Mainly dot, say and test.

; iter ;
;;; ===========================================================================
(define-library (stsq prelib)
  (export
    files-for-restart/scm
    files-for-restart/lib
    files-for-reload/scm
    files-for-reload/lib

    ifnot
    ok
    is?
    further
    odds-on
    assert
    partial

    clr tab spc LF lf lff goto vtab vtab-up vtab-down
    set-lf-indentation get-lf-indentation clear-lf-indentation
    print println print_ print!

    dot-as-std
    dot-as-section
    dot-as-text
    dot-as-comment
    dot-as-prompt
    dot-as-dbg
    dot-as-err
    section
    set-terminal-title

    print-callstack
    print-err-object
    exit-ok
    exit-fail
    err roadwork SWW

    say say? __say/v
    test tests-count tests-counter notify-if-some-tests-failed)

  (import
    (scheme base)
    (scheme write)
    (scheme eval)
    (scheme time)
    (scheme process-context)  ; (exit)
    (tr7 environment)
    (tr7 foreigns)
    (tr7 debug))

  (begin
;;; ===========================================================================
;;; :chapter: GLOBALS
    (define files-for-restart/scm       '())
    (define files-for-restart/lib       '())
    (define files-for-reload/scm        '())
    (define files-for-reload/lib        '())


;;; ===========================================================================
;;; :chapter: MACROS
    (define-syntax ifnot                                                        ; #DOC #MAC if not ...
      (syntax-rules ()
        ((ifnot test iftrue iffalse)
         (if test iffalse iftrue))))

    (define ok 'ok)                                                             ; #DOC return ok

    (define-syntax is?                                                          ; #DOC #MAC
      (syntax-rules ()
        ((_ x) (if x #true #false))))

    (define-syntax further             ; #MAC #DOC Return (f x) if (test x) == #true. Else return x unchanged.
      (syntax-rules ()
        ((_ x test f) (if (test x) (f x) x))))

    (define-syntax odds-on                             ; #MAC #DOC Return (f ...). If that fails return #false.
      (syntax-rules ()
        ((_ f ...) (guard (e (else #f)) (f ...)))))

    (define (.exit/ass location test)
       (lf) (dot-as-err)
       (print "Assertion failed: ") (print location)
       (lf) (print test)
       (dot-as-std) (lf)
       (exit-fail))

    (define-syntax assert                                               ; #DOC #MAC (assert location test1 ...)
       (syntax-rules ()
          ((_ location test ...)
             (let ((t (and test ...)))  (if t t (.exit/ass location '(test ...)))))))


   (define-syntax partial                          ; #COMBINATORS #DOC Return lambda: (apply f ... args)
     (syntax-rules ()
       ((_ f ...) (lambda args (apply f ... args)))))


;;; ===========================================================================
;;; :chapter: DOT
;;; :section:   basics
    (define at-line-start #t)
    (define indentation   0)
    (define (?indent) (unless (zero? indentation) (tab indentation)))
    (define (set-lf-indentation col) (set! indentation col))                    ; #DOT #DOC
    (define (get-lf-indentation) indentation)                                   ; #DOT #DOC
    (define (clear-lf-indentation) (set-lf-indentation 0))                      ; #DOT #DOC

    (define (csi) (write-char #\escape) (write-char #\[)) ; contol sequence inducer

    (define (clr)                                                               ; #DOC Clear screen. #DOT
      (csi) (display "2J")             ; clear screen
      (csi) (display "H")              ; goto home position (1/1)
      (set! at-line-start #t)
      ok)
    (define (tab col)                  ; #DOC Move cursor to column in current line. Forward or backward. #DOT
      (let ((c (integer->char (+ 48 (quotient col 10))))
            (d (integer->char (+ 48 (remainder col 10)))))
        (csi) (write-char c) (write-char d) (write-char #\G)
        (display " |")
        ""   ; !!! say prints the value returned by tab, we want nothing there -> return ""
        ))

    (define (goto line col)                                    ; #DOT #DOC cursor goto line/col. up-left is 1/1
      (define cout write-char)
      (let ((c (integer->char (+ 48 (quotient line 10))))
            (d (integer->char (+ 48 (remainder line 10))))
            (e (integer->char (+ 48 (quotient col 10))))
            (f (integer->char (+ 48 (remainder col 10)))))
        (csi) (cout c) (cout d) (display ";") (cout e) (cout f) (cout #\H)))

    (define (vtab line) (goto line 0) (?indent))                                ; #DOT #DOC (goto line 0)

    (define (vtab-up lines)                                                    ; #DOT #DOC Move cursor lines up
      (let ((c (integer->char (+ 48 (quotient lines 10))))
            (d (integer->char (+ 48 (remainder lines 10)))))
        (csi) (write-char c) (write-char d) (write-char #\A))
      (?indent))

    (define (vtab-down lines)                                                ; #DOT #DOC Move cursor lines down
      (let ((c (integer->char (+ 48 (quotient lines 10))))
            (d (integer->char (+ 48 (remainder lines 10)))))
        (csi) (write-char c) (write-char d) (write-char #\B))
      (?indent))

    (define (spc) (write-char #\space))                                         ; #DOC #DOT
    (define (LF)  (newline) (?indent))                                          ; #DOC #DOT
    (define (lff) (lf) (LF) ok)                                                 ; #DOC #DOT
    (define (lf)                                                                ; #DOC #DOT
      (unless at-line-start
        (set! at-line-start #t)
        (LF)))

    (define (print x) (display x) (set! at-line-start #f))                      ; #DOC #DOT
    (define (println x) (print x) (lf))                                         ; #DOC #DOT
    (define (print_ x) (print x) (spc))                                         ; #DOC #DOT
    (define (print! x) (print x) (flush-output-port))                           ; #DOC #DOT


;;; ---------------------------------------------------------------------------
;;; :section:   color and style
    (define GRAY                "128;128;128")                                  ; #COLOR
    (define DARKGRAY            "96;96;96")                                     ; #COLOR
    (define WHITE               "255;255;255")                                  ; #COLOR
    (define YELLOW              "240;200;100")                                  ; #COLOR
    (define RED                 "255;0;0")                                      ; #COLOR
    (define PINK                "255;60;170")                                   ; #COLOR
    (define SKY                 "80;128;230")                                   ; #COLOR

    (define (dot-as-std)        (change-fg 'std WHITE))
    (define (dot-as-section)    (change-fg 'section YELLOW))
    (define (dot-as-text)       (change-fg 'text GRAY))
    (define (dot-as-comment)    (change-fg 'comment DARKGRAY))
    (define (dot-as-prompt)     (change-fg 'prompt SKY))
    (define (dot-as-dbg)        (change-fg 'dbg PINK))
    (define (dot-as-err)        (change-fg 'err RED))

    (define current-fg          'std)

    (define (change-fg name color)
      (unless (eq? current-fg name)
        (set! current-fg name)
        (dot-in color)))

    (define (dot-in color) (csi) (display "38;2;") (display color) (write-char #\m))
    (define (dot-on color) (csi) (display "48;2;") (display color) (write-char #\m))

    (define (section headline)                                                  ; #DOC #DOT
      (lf) (dot-as-section) (print "------------  ") (print headline) (dot-as-text)
      (lf))

    (define (set-terminal-title mess)                                           ; #DOC #DOT
        (write-char #\x1B)
        (display "]0;")
        (display mess)
        (write-char #\x07))


;;; ---------------------------------------------------------------------------
;;; :section:   errmess and exit/halt
    (define (.print-callstack-entry e)
      (let-values (((fun args file line) (apply values e)))
        (lf) (print line) (spc) (print file)
        (tab 32) (print fun)
        (spc) (write args)))

    (define (print-callstack)                                                   ; #DOC #ERR
      (dot-as-dbg)
      (lff) (for-each .print-callstack-entry (reverse (tr7-call-stack))))

    (define (print-err-object from item)                                        ; #DOC #ERR
      (dot-as-err)
      (LF) (print "############  Scheme Error")
      (lf) (print "from: ") (print from)
      (LF) (print "mess: ") (print (error-object-message item))
      (LF)
      (for-each println (error-object-irritants item))
      (dot-as-text))

    (define (do-error name mess values)
       (lff) (dot-as-err) (print "############  ") (print name)
       (lf) (print mess)
       (lf) (for-each println values)
       (exit-fail))


    (define (exit-ok)   (dot-as-std) (lff) (exit #true))                        ; #DOC #ERR #EXIT
    (define (exit-fail) (dot-as-std) (lff) (exit #false))                       ; #DOC #ERR #EXIT

    (define (err mess . values)         (do-error "ERROR" mess values))         ; #DOC #ERR #EXIT
    (define (roadwork mess . values)    (do-error "ROADWORK" mess values))      ; #DOC #ERR #EXIT
    (define (SWW mess . values)         (do-error "SWW" mess values))           ; #DOC #ERR #EXIT


;;; ===========================================================================
;;; :chapter: SAY  --  adopted form https://kittenlang.org
(define sayvar/lastform  'sayvar/lastform)
(define sayvar/lastvalue 'sayvar/lastvalue)

(define (do-say-cmd x)
   (cond
      ((eq? x '/===)  (lff) (dot-as-section) (print "============  "))
      ((eq? x '===/)  (lf))
      ((eq? x '//)    (lff))
      ((eq? x '/)     (lf))
      ((eq? x '_)          (print "    "))
      ((eq? x '/_)    (lf) (print "    "))
      ((eq? x '!!)    (flush-output-port))
      ((eq? x '~)     (print_ sayvar/lastform))
      ((eq? x 'CMT)   (dot-as-comment))
      ((eq? x 'DBG)   (dot-as-dbg))
      ((eq? x 'ERR)   (dot-as-err))
      ((eq? x 'PMT)   (dot-as-prompt))
      ((eq? x 'STD)   (dot-as-std))
      ((eq? x 'TXT)   (dot-as-text))
      (else            (print x) (SWW "do-say-cmd missed a valid exit"))))

(define-syntax say                                                              ; #DOC #SAY
   (syntax-rules ()
      ((_ x ...)
         (begin
           (cond
             ((and (symbol? 'x)
                   (member  'x  '(/=== ===/ // / _ /_ !! ~ CMT DBG ERR PMT STD TXT)))
                (do-say-cmd 'x))

             ((and (pair? 'x) (eq? (car 'x) 'quote))
                (print_ 'x))

             (else
                (set! sayvar/lastform  'x)
;                 (set! sayvar/lastvalue (eval 'x (current-environment))) ; macros & quotes are a tricky thing
;                 (set! sayvar/lastvalue (eval x (current-environment))) ; macros & quotes are a tricky thing
;                 (set! sayvar/lastvalue (eval 'x)) ; macros & quotes are a tricky thing
;                 (set! sayvar/lastvalue (eval x)) ; macros & quotes are a tricky thing
                (set! sayvar/lastvalue x) ; macros & quotes are a tricky thing ... finally ...
                (print_ sayvar/lastvalue))
             )
             ...
             (dot-as-text)
             ))))

(define (say?)                                                                  ; #DOC #SAY #HELP
   (say /=== 'say-this )
   (say /_ "//  /  _  /_     CMT DBG ERR PMT STD TXT")
   (say /_ "/=== <headline>")
   (say /_ "/=== <headline> ===/")
   (say /_ "!!                                ; flush-output-port")
   (say /_ "~                                 ; print this last form")
   (say /_ "<text> (tab 24) <more-text>       ; does the indentation")
   (say /_ "(clr)                             ; does a clear screen")
   (say /_ "(now)                             ; print current time")
   (lff))

(define (__say/v)
   (let ((a 10)
         (b 100)
         (c 1000))
   (say /=== 'headline)
   (say /"This is a string.")
   (say /_ "Next line indented.")
   (say / CMT 'comment DBG 'debug ERR 'error PMT 'prompt STD 'standard TXT 'text)
   (say / (+ 1 2 3 4) ~   _ a ~   b ~   c ~   _   (+ a b c) ~)
   (say //)
   #t))


;;; ===========================================================================
;;; :chapter: TESTS
(define tests-count 156)                        ; #DOC #TEST So many tests we have.
(define tests-counter 0)                        ; #DOC #TEST So many test are done.
(define tests-result #t)

(define (notify-if-some-tests-failed)           ; #DOC #TEST In case failed test message scrolled off screen
      (unless (eqv? tests-count tests-counter)
        (lf) (print "    ") (dot-as-dbg)
        (print tests-counter) (print " of ") (print tests-count) (print " tests done.")
        (dot-as-text) (lf))
      (unless tests-result
        (lff) (dot-as-err) (print "############ Some tests failed.")
        (lff) (dot-as-text)))

(define-syntax test                             ; #DOC #TEST
      (syntax-rules ()
        ((test s t r)                  ; Symbol Test expected-Result
         (let ((res t))
           (set! tests-counter (+ 1 tests-counter))
           (unless (equal? res r)
             (display "\x1b;[38;2;255;60;170m")
             (newline) (display "-----  TEST FAILED:  '") (write s)
             (newline) (display "test:     ") (write 't)
             (newline) (display "expected: ") (write 'r)
             (newline) (display "got:      ") (write res)
             (display "\x1b;[38;2;255;255;255m")
             (newline)
             (set! tests-result #f)
             )))))


;;; ===========================================================================
(print! 'prelib/)
)) ; library begin
;;; :file: END sts-q 2023-Sep
