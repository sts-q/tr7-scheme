;;; ===========================================================================
;;; :file: bit.sld             Bitwise operations.

; =============================================================================
(define-library (stsq bit)
  (export
    bit-and bit-or bit-shl bit-asr      ; re-export from (tr7 foreign)
    bit-members
    bit-count)
  (import
    (scheme base)
    (tr7 foreigns)
    (scheme eval)
    (tr7 environment)
    (stsq prelib)
    (stsq lib))
  (include "bit.scm")
  (include "bit-test.scm")
  (begin (__bit)))

   
;;;  ==========================================================================
;;; :file: END sts-q 2023-Sep

