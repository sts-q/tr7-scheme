; =============================================================================
(define (__repl/v)
  (section "repl")
  (lf) (print "first-prompt?: ") (print first-prompt?)
  (lf) (print "restart:") (for-each (lambda (f) (lf) (print (file-exists? f)) (spc) (print f)) (restart-files))
  (lff)
  (lf) (print "reload:") (for-each (lambda (f) (lf) (print (file-exists? f)) (spc) (print f)) (reload-files))
  (lff))


; =============================================================================
