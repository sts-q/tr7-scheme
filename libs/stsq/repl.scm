; =============================================================================
; :file: repl.scm               ; scm-repl


; main init iter say ;
; =============================================================================
(define-library (stsq repl)
  (export
    restart-files
    reload-files
    ?reload-libs
    repl-init
    repl
    iter
    __repl/v)

  (import
    (scheme base)
    (scheme read)
    (scheme write)
    (scheme file)
    (scheme eval)
    (scheme process-context)
    (tr7 environment)
    (tr7 misc)
    (stsq prelib)
    (stsq lib)
    (stsq ddx))

  (begin
    ; =============================================================================
    ; :chapter: LIB
    (define (print-scm-repl-help)
      (say /=== "scm-repl  Seven for TR7 version" (tr7-version))
      (print  "\n    ?           print help about this repl")
      (print  "\n    !           print Seven/TR7 greeting")
      (print  "\n    <Ctrl-d>    exit to shell prompt")
      (print  "\n    <  >        2 spaces: clear screen")
      (print  "\n    ..          exit to shell prompt")
      (print  "\n    .           eval '(scratch)")
      (print  "\n    :           eval '(main)")
      (print  "\n    <enter>     At new version info:  exit to shell prompt.")
      (print  "\n    <enter>     At start of repl:     exec last command from history.")
      (print  "\n    (say?)      Print help about 'say'.")
      (print  "\n    (help)      Print more help.")
      (lf)
      #t
      )

    (define iterfile            (path-home "r/iterfile"))
    (define ledit-history-file  (path-home "r/ledithist-tr7"))
    (define seven-history-file  (path-home ".seven_history"))
    (define bin-file            (path-pwd "seven"))
    (define first-prompt?       #true)

    (define (restart-files)
      (let* ((fs/scm files-for-restart/scm)
             (fs/lib files-for-restart/lib)
             (file/scm (lambda (name) (path-pwd "scm/" name ".scm")))
             (file/lib (lambda (name) (path-pwd "libs/stsq/" name ".scm"))))
        (append (map file/scm fs/scm) (map file/lib fs/lib))))

    (define (reload-files)
      (let* ((fs/scm files-for-reload/scm)
             (fs/scmdata ())
             (fs/lib files-for-reload/lib)
             (file/scm (lambda (name) (path-pwd "scm/" name ".scm")))
             (file/lib (lambda (name) (path-pwd "libs/stsq/" name ".scm")))
             (file/data (lambda (name) (path-pwd "scm/" name ".scmdata"))))
        (append (map file/scm fs/scm) (map file/lib fs/lib) (map file/data fs/scmdata))))

    (define last-cmd
      (letrec ((is-valid? (lambda (l) (guard (e (else #f)) (read (open-input-string l)) #t)))
               (is-template? (lambda (l)
                              (cond
                                ((string=? l "..")      #true)
                                ((string=? l "?")       #true)
                                ((string=? l "!")       #true)
                                ((string=? l ".")       #true)
                                ((string=? l ":")       #true)
                                ((string=? l "")        #true)
                                ((string=? l "  ")      #true)
                                (else                   #false))))
               (first-valid (lambda (lines)
                              (cond
                                ((null? lines)            #false)
                                ((is-template? (car lines)) (first-valid (cdr lines)))
                                ((is-valid? (car lines))  (car lines))
                                (else                     (first-valid (cdr lines)))))))
;         (if (file-exists? ledit-history-file)                                 ; ledit
;             (first-valid (reverse (file-read-lines ledit-history-file)))
;             #false)
        (if (file-exists? seven-history-file)                                   ; rlwrap
            (first-valid (reverse (file-read-lines seven-history-file)))
            #false)))

    (define (info-about-tests)
      (lf) (dot-as-dbg)
      (print tests-counter) (print "/") (print tests-count) (print " tests done.")
      (dot-as-text))

   (define (repl-init files)
      (?reload-libs)
      #t)


    ; -----------------------------------------------------------------------------
    ; :section:     Newer versions?
    (define (check-for-newer-versions)
      (define (touched? f)
        (if (file-touched? f)
            (begin
               (lf) (dot-as-dbg) (print "New version?  ") (dot-as-text) (print (path-file-name f))
               (lf)
               #t)
            #f))
      (define (loop fs)
        (cond
          ((null? fs) #f)
          ((file-touched? (car fs))
             (lf) (dot-as-dbg) (print "New version?  ") (dot-as-text) (print (path-file-name (car fs))) #t)
          (else (loop (cdr fs)))))
      (or (loop (restart-files)) (touched? bin-file)))

    (define (?reload-libs)
      (define success #t)
      (letrec ((reload (lambda (f)
                         (lf) (dot-as-dbg) (print "Reloading new version:   ") (dot-as-text)
                         (print (path-file-name f)) (print "   ")
                         (set! success (and success (file-load f)))
                         (lf)))
               (loop (lambda (fs)
                       (cond
                         ((null? fs)                 #f)
                         ((file-touched? (car fs))   (reload (car fs)) (loop (cdr fs)))
                         (else                       (loop (cdr fs)))))))
        (loop (reload-files)))
      success)


    ; -----------------------------------------------------------------------------
    ; :section:     Prompt
    (define (read-prompt prompt)
      (LF) (dot-as-prompt) (print prompt) (dot-as-std) (flush-output-port)
      (let ((line (read-line)))
        (dot-as-text)
        (set! at-line-start #t)
        line))

    (define (eval-string line)
      (guard (e (else (if (error-object? e)
                          (print-err-object 'repl e)
                          (begin (lff) (dot-as-err) (print "############  Unkown execption.")))))
             (letrec ((p (open-input-string line))
                      (exec (lambda (f)
                              (let-values (((r . rest) (eval f)))
                                (write-shared r)
                                (if (list? rest)
                                   (for-each (lambda (x) (LF) (write-shared x)) rest))
                                )))
                      (loop (lambda (?lf)
                              (let ((f (read p)))
                                (cond
                                  ((eof-object? f) #t)
                                  (else (when ?lf (LF) (print "> "))
                                        (exec f)
                                        (loop #t)))))))
               (loop #f))))

    (define (eval-last-prompt)
      (set! first-prompt? #f)
      (print last-cmd) (lf)
      (eval-string last-cmd))


    ; -----------------------------------------------------------------------------
    ; :section:     repl
    (define (repl)                     ; #DOC Start scm-repl.
      (define news    (check-for-newer-versions))
      (define (quit)  (LF) (exit))
      (when (and first-prompt? last-cmd)
        (lf) (print "last cmd:  ") (print last-cmd))
      (let ((line (read-prompt "seven> ")))
        (cond ((eof-object? line)               ; quit Ctrl-d
               (quit))

              ((string=? ".." line)             ; quit '..'
               (quit))

              ((string=? ":" line)              ; eval '(main)
               (clr)
               (set! first-prompt? #f)
               (?reload-libs)
               (eval '(main))
               (lf) (print (now))
               (repl))

              ((string=? "." line)              ; eval '(scratch)
               (clr)
               (set! first-prompt? #f)
               (?reload-libs)
               (eval '(scratch))
               (lf) (print (now))
               (repl))

              ((and news (string=? "" line))    ; quit on new version on empty line
               (quit))

              ((and first-prompt? (string=? "" line)) ; eval last prompt
               (?reload-libs)
               (eval-last-prompt)
               (repl))

              ((string=? "" line)               ; empty prompt
               (?reload-libs)
               (repl))

              ((string=? "  " line)             ; clear srceen
               (clr)
               (repl))

              ((string=? "?" line)              ; help
               (print-scm-repl-help)
               (repl))

              ((string=? "!" line)              ; help
               (hello?)
               (repl))

              (else                             ; eval prompt line
               (?reload-libs)
               (set! first-prompt? #false)
               (eval-string line)
               (repl)))))


; -----------------------------------------------------------------------------
; :section:   iter
    (define (iter)                     ; #DOC hmm still not really working...
      (define (wait)
         (msleep 250)
         )
      (define (done)   (print " --- ") (print (now)) (print " --- ") (write-char #\return) (flush-output-port))
      (cond
         ((file-exists? iterfile)
            (clr)                     ; clear screen
            (delete-file iterfile)
            (if (check-for-newer-versions)
               (exit))
            (if (?reload-libs)
               (begin
;                   (let ((file (path-pwd "scm/scratch.scm")))
;                      (file-load file))
                  (eval '(scratch) (current-environment))
                  (done)
                  (iter))
               (begin
                  (lf) (print "############ ?reload failed") (lf)
                  (msleep 2000)
                  (iter))))
         (else
            (wait)
            (iter))))


; -----------------------------------------------------------------------------
    (?reload-libs)          ; get a first touchtime for all libs (that is: now)
    (print! 'repl/)
; =============================================================================
    ) ; begin
  (include "repl-test.scm")
  ) ; define-library
; =============================================================================
; :file: END sts-q 2023-Sep

