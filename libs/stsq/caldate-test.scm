; =============================================================================
; :file: caldate-test.scm

(define (__caldate)
  (test 'valid-daynum? (valid-daynum? -1) #f)
  (test 'valid-daynum? (valid-daynum? (+ 1 last-valid-daynum)) #f)
  (test 'valid-daynum? (valid-daynum? 12) #t)
  ;; 
  (test 'leapyear? (leapyear? 2000) #t)
  (test 'leapyear? (leapyear? 2001) #f)
  (test 'leapyear? (leapyear? 2003) #f)
  (test 'leapyear? (leapyear? 2004) #t)
  ;; 
  (test 'month-length (month-length 2000 1) 31)
  (test 'month-length (month-length 2000 2) 29)
  (test 'month-length (month-length 2001 2) 28)
  (test 'month-length (month-length 2003 2) 28)
  (test 'month-length (month-length 2004 2) 29)
  (test 'month-length (month-length 1912 12) 31)
  ;; 
  (test 'valid-caldate? (valid-caldate? 2000 1 31) #t)
  (test 'valid-caldate? (valid-caldate? 2000 2 29) #t)
  (test 'valid-caldate? (valid-caldate? 2001 2 28) #t)
  (test 'valid-caldate? (valid-caldate? 2002 2 28) #t)
  (test 'valid-caldate? (valid-caldate? 2003 2 29) #f)
  (test 'valid-caldate? (valid-caldate? 2004 2 29) #t)
  (test 'valid-caldate? (valid-caldate? 1912 4 30) #t)
  (test 'valid-caldate? (valid-caldate? 1912 4 31) #f)
  ;; 
  (test 'read-caldate-iso (read-caldate-iso "2004-12-24") '(2004 12 24))
  ;; 
  (test 'caldate->daynum (caldate->daynum '(1900 1 1)) 0)
  (test 'caldate->daynum (caldate->daynum '(1900 1 15)) 14)
  (test 'caldate->daynum (caldate->daynum '(1900 2 10)) 40)
  (test 'caldate->daynum (caldate->daynum '(1901 3 1)) 424)
  (test 'caldate->daynum (caldate->daynum '(1902 10 20)) 1022)
  (test 'caldate->daynum (caldate->daynum '(2023 06 21)) 45096)
  ;; 
  (test 'daynum->caldate (daynum->caldate 0) '(1900 1 1))
  (test 'daynum->caldate (daynum->caldate 10) '(1900 1 11))
  (test 'daynum->caldate (daynum->caldate 40) '(1900 2 10))
  (test 'daynum->caldate (daynum->caldate 400) '(1901 2 5))
  ;; 
  (test 'daynum->weekday (show-weekday (daynum->weekday (caldate->daynum '(2000 1 1)))) "Sat")
  ;; 
  (test 'caldate-diff (caldate-diff '(2023 12 24) '(2023 09 28)) 87)
  ;; 
)

(define (__caldate/v)
  (section "__caldate")
  (letrec ((loop (lambda (dn)
                    (cond ((> dn last-valid-daynum) (values))
                          (else (check dn) (loop (+ 1 dn))))))
            (check (lambda (dn)
                     (let* ((cd (daynum->caldate dn))
                              (dn2 (caldate->daynum cd)))
                       (unless (= dn dn2) (err "__caldate: dn # dn2" (list dn cd dn2)))
                       (out dn cd dn2))))
            (out (lambda (dn cd dn2)
                   (lf) (print (show-int 6 dn)) (print (show-int 6 dn2)) (spc) (print cd))))
    (loop 0)
    (lff)
    (lf) (print (current-daynum)) (print (current-caldate)) (spc) (print (today-now))
    (lff)))


; =============================================================================
; :file: END sts-q 2023-Sep

