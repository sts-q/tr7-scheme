;;; ===========================================================================
;;; :file: csv-test.scm

(define (__csv)
  (test 'csv-strings (csv-strings "hallo;this;is;a;string;") '("hallo" "this" "is" "a" "string" ""))

)

;;; ===========================================================================
;;; :file: END sts-q 2023-Sep

