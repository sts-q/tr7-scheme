; =============================================================================
; :file: bit.scm ; Bitwise operations

; =============================================================================
; :chapter: LIB
(define integer-bit-size 32)                                                    ; #DOC

; =============================================================================
; :chapter: MAIN
(define (bit-members n)                    ; #DOC Return list of integers indicating the positions of set bits.
  (letrec ((f    (lambda (i)  (not (zero? (bit-and n (bit-shl 1 i))))))
           (loop (lambda (i)  (cond
                               ((< i 0)   '())
                               ((f i)     (cons i (loop (1- i))))
                               (else      (loop (1- i)))))))
    (loop (1- integer-bit-size))))


(define (bit-count n)    (length (bit-members n)))                              ; #DOC


; =============================================================================
(print! 'bit/)
; :file: END sts-q 2023-Sep
