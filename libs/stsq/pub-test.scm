;;; ===========================================================================
;;; :file: pub-test.scm


;;; ===========================================================================
(define (__pub/v)
   (say /=== '__pub/v ===/)
   (xprint
      (xhtml
      (xhead
         (xtitle "A first html")
         )
         (xbody
            (xh1 "chapter 1")
            (xh1 "section 1.1")
            (xp   "Some text here." 'in-a-paragraph 12000)

            (xh1 "chapter 2")
            (xh2 "section 2.1")
            (xh2 'section-2.2)
            (xp   "More text to show here..." "Next sentence..." 12)
            (xp   "More text to show here..." "Next sentence..." 12)

            (xchapter 'more-to-come)
            )))

   (say /=== '__pub/v ===/))

;;; ===========================================================================
;;; :file: END sts-q 2024-Aug
