; =============================================================================
; :chapter: DOCTAB  (  .
; with command foreign syntax environment time ;
(define *doctab* '(
; -----------------------------------------------------------------------------
; :section:     R7RS
   ((scheme base) c "" (
        ((car l)                                        "return first element of list, fail on ()")
        ((cdr l)                                        "return rest of list, fail on ()")
        ; ...
        ))
   ((scheme case-lambda) c "" (
        (case-lambda syntax                             "")
        ))
   ((scheme char) c "" (
        ((char-alphabetic? c)                           "")      
        ; ...
        ((string-upcase str)                            "Upcase string.")
        ))
   ((scheme complex) c "" ())
   ((scheme cxr) c "" (
        ((caaaar l)                                     "")
        ; ...
        ((cdddr l)                                      "")
        ))
   ((scheme eval) c "Procedures for evaluating scheme data as programs." (
        ((environment ?)                                "")
        ((eval ?)                                       "")
        ))
   ((scheme file) c "" (
        ((call-with-input-file ..)                      "")
        ((delete-file ..)                               "")
        ((open-binary-input-file ..)                    "")
        ((open-input-file ..)                           "")
        ((with-input-from-file ..)                      "")
        ((call-with-output-file ..)                     "")
        ((file-exists? ..)                              "")
        ((open-binary-output-file ..)                   "")
        ((open-output-file ..)                          "")
        ((with-output-to-file string thunk)             "call thunk(no-args) with current-output-port := file")
        ))
   ((scheme inexact) c "Functions on inexact values: sin cos sqrt ..." ())
   ((scheme lazy)    c "Lazy evaluation."
        ((delay)                                        "")
        ((force)        "")
        ((promise?)     "")
        ((delay-force)  "")
        ((make-promise) "")
        )
   ((scheme load) c "Loading scheme expressions from file." (
        ((load ..)                                      "")
        ))
   ((scheme process-context) c "Command line, environment vars. Program exit." (
        ((command-line)                                 "return list of strings with command line args")
        ((get-environment-variable string)              "return value of environment variable")
        ((get-environment-variables)                    "return list of name-value-pairs of environment")
        ((emergency-exit [obj])                            "     ")
        ((exit [obj])                                   "#t|0 -> success; #f|1 > failed")
        ))
   ((scheme read) c "Reading scheme objects, form string or file." (
        ((read ..)                                      "")
        ))
   ((scheme repl) c "Export interaction-environment." (
        ((interaction-environment ..)                   "")
        ))
   ((scheme time) c "Time related values: current second, jiffies." (
        ((current-jiffy ..)                             "")
        ((jiffies-per-second ..)                        "")
        ((current-second)                               "")
        ))
   ((scheme write) c "Writing scheme objects." (
        ((display ..)                                   "")
        ((write-shared ..)                              "")
        ((write ..)                                     "")
        ((write-simple ..)                              "")
        ))
; -----------------------------------------------------------------------------
; :section:     non standard
   ((scheme box)   c   "srfi 111" ())
   ((srfi 136)     c   "Extensible record types."
        ((record? obj)                                  "return #t if and only if obj is constructed by this")
        ((record-type-descriptor? obj)                  "")
        ((record-type-descrptor record)                 "")
        ((record-type-predicate rtd)                    "")
        ((record-type-name rtd)                         "")
        ((record-type-parent rtd)                       "")
        ((record-type-fields rtd)                       "")
        ((make-record-type-descriptor name fieldspecs)  "")
        ((make-record-type-descriptor name fieldspec parent) "")
        ((make-record rtd field-vector)                 "")
        )
; -----------------------------------------------------------------------------
; :section:     tr7
   ((tr7 extra) c "Cool must-haves for any List-Processor!"  (
        ((car+cdr pair)                                "returns values car and cdr")
        ((length* lst)                                 "return -1 if circular, length for proper, -1 for pair")
        ((append-reverse lst tail)                      "== (append (reverse lst) tail)")
        ((append-reverse! lst tail)                     "== (append! (reverse! lst) tail)")
        ((cons* elt1 elt2 ...)                          "Like list, but the last arg as tail.")
        ))
   ((tr7 environment) c "Named spaces?" (
        ((environment x)                                "is x an environment?")
        ((defined? symbol [env])                         "if symbol attached to a value in ENV def: current")
        ((symbols-set)                                  "return list of known symbols")
        ((current-environment)                          "the current environment")
        ((tr7-environment->list [ENV [DEPTH]])          "???")
        ))
   ((tr7 extension) c "Extension of the c part." (
        ((load-extension str [LIBNAME])                 "load shared library of path str in lib LIBNAME")
        ))
   ((tr7 foreigns) c "Extension of the c part, implemented extern, here in seven.c" (
        ((system command)                               "Run shell command")
        ((myadd number number)                          "add two numbers")
        ((xterm-width)                                  "return terminal window width")
        ((xterm-height)                                 "return terminal window height")
        ((nanosleep number number)                      "sleep seconds nanoseconds")
        ((file-touch-time string)                       "return file touch time [seconds]")
        ((bit-shl number number)                        "bit shift left")
        ((bit-asr number number)                        "bit arithmetic shift right")
        ((bit-and number ...)                           "bit and: init := -1")
        ((bit-or number ...)                            "bit or: init := 0")
        ))
   ((tr7 gc) c "GC" (
        ((tr7-gc [VERBOSE])                             "perform a gc run")
        ((tr7-gc-verbose [VERBOSE])                     "")
        ((new-segment [COUNT])                          "allocate count segments, default: 1")
        ))
   ((tr7 trace) c "Tracing" ( 
        ((tr7-tracing num)                              "")
        ((tr7-show-prompt [BOOL])                       "")
        ((tr7-show-eval [BOOL])                         "")
        ((tr7-show-compile [BOOL])                      "")
        ((tr7-show-result [BOOL])                       "")
        ))
   ((tr7 debug) c "Debugging aids." (
        ((tr7-call-stack)                               "return representation of current calling stack")
        ((tr7-exec-stack)                               "return representation of current execution stack")
        ((error-object-stack err)                       "return representation of stack when error occured")
        ))
; -----------------------------------------------------------------------------
; :section:     libs/srfi
   ((srfi 1)            libs/scheme     "(scheme list)" ())
   ((srfi 69)           libs/srfi       "hash tables" ())
   ((srfi 111)          libs/srfi       "boxes" ())
   ((srfi 141)          libs/srfi       "division" ())
; -----------------------------------------------------------------------------
; :section:     libs/scheme
   ((scheme box)        libs/scheme     "???" ())
   ((scheme division)   libs/scheme     "SRFI-141: ceiling floor truncate round ..." ())
   ((scheme list)       libs/scheme     "(srfi 1) Olin Shivers list lib from 1999." ())
   ((scheme r5rs)       libs/scheme     "R5RS" ())

; -----------------------------------------------------------------------------
; :section:     wishlist
   ((srfi 169)          done            "Underscores in numbers: working with notation: #d_12_000" ())
; -----------------------------------------------------------------------------
; :section:     sts-q
   ((stsq bit)          libs/stsq       "Bitwise operations." ())
   ((stsq caldate)      libs/stsq       "Calender date operations." ())
   ((stsq csv)          libs/stsq       "CSV values." ())
   ((stsq ddx)          libs/stsq       "Dot documentation." ())
   ((stsq lib)          libs/stsq       "Pervasive procedures." ())
   ((stsq prelib)       libs/stsq       "Pervasive print procedures." ())
   ((stsq pub)          libs/stsq       "Make html files." ())
   ((stsq random)       libs/stsq       "Random numbers." ())
   ((stsq repl)         libs/stsq       "Scheme read-eval-print-loop in scheme." ())
   ((stsq tt)           libs/stsq       "TTs" ())
   ((stsq xmlgen)       libs/stsq       "Make xml files." ())
; -----------------------------------------------------------------------------
   )) ; define *doctab* (..)

   
