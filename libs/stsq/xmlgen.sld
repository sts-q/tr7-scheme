;;; ===========================================================================
;;; :file: xmlgen.sld  --  Building xml documents

; =============================================================================
(define-library (stsq xmlgen)
  (export
      xatt xatt? xatt-show xatts-show
      xtag xatt? xtag-show xtag-empty?
      __xmlgen/v)

  (import
    (scheme base)
    (tr7 foreigns)
    (scheme eval)
    (tr7 environment)
    (stsq prelib)
    (stsq lib))

  (include "xmlgen.scm")
  (include "xmlgen-test.scm")

  (begin
     #t))


;;;  ==========================================================================
;;; :file: END sts-q 2024-Aug
