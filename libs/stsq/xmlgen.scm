; =============================================================================
; :file: xmlgen.scm   --   Building xml documents.

; =============================================================================
; :chapter: LIB
(define (valid-item? x)                       ; valid items for attribute-values and tag-contents
   (or (string?  x)
       (symbol?  x)
       (integer? x)))

(define (show-item x)
   (cond
      ((string? x)      x)
      ((symbol? x)      (symbol->string x))
      ((integer? x)     (show-int 0 x))
      (else             (SWW "show-item missed a cond-case"))
      ))

(define lt   (string->list "&lt;"))
(define gt   (string->list "&gt;"))
(define amp  (string->list "&amp;"))
(define quot (string->list "&quot;"))
(define apos (string->list "&apos;"))

(define (translate cs)
   (cond
      ((null? cs)    '())
      (else
         (let* ((c  (car cs))
                (cs (cdr cs)))
            (append (cond
                        ((eq? c #\<)     lt)
                        ((eq? c #\>)     gt)
                        ((eq? c #\&)     amp)
                        ((eq? c #\")     quot)
                        ((eq? c #\')     apos)
                        (else            (list c)))
                    (translate cs))))))

(define (translate-special-chars str)
   (assert 'translate-special-chars (string? str))
   ((pipe string->list translate list->string) str))


; =============================================================================
; :chapter: ATTRIBUTES
(define-record-type <xml-attribute> (make-xatt name value) xatt?                ; #DOC #XMLGEN
   (name   xatt-name)
   (value  xatt-value))

(define (xatt name value)                                                       ; #DOC #XMLGEN
   (assert (list 'xatt: name value) (symbol? name) (valid-item? value))
   (make-xatt name value))

(define (xatt-show a)
   (string-append " " (symbol->string (xatt-name a)) "=\"" (show-item (xatt-value a)) "\""))

(define (xatts-show as)                                                         ; #DOC #XMLGEN
   (apply string-append (map xatt-show as)))


; =============================================================================
; :chapter: TAGS
(define-record-type <xml-tag> (make-xtag name atts tags) xtag?                  ; #DOC #XMLGEN
   (name xtag-name)
   (atts xtag-atts)
   (tags xtag-tags))

(define (xtag name atts tags)
   (assert (list 'xtag: name)
      (symbol? name)
      (list?   atts)
      (all?    xatt? atts)
      (or (eq? #f tags)
          (and (list? tags) (all? (or/ps xtag? valid-item?) tags))) )
   (make-xtag name atts tags))

(define (xtag-empty? t)                                                         ; #DOC #XMLGEN
   (and (xtag? t) (eq? #f (xtag-tags t))))

(define (show-tags ts)
   (apply string-append (map xtag-show ts)))

(define (show-tag-tag tag)
   (let* ((name         (symbol->string (xtag-name  tag)))
          (atts         (xatts-show     (xtag-atts  tag)))
          (content      (show-tags      (xtag-tags  tag))))
   (string-append "\n<"name atts">" content "</"name">")))

(define (show-empty-tag tag)
   (let* ((name         (symbol->string (xtag-name  tag)))
          (atts         (xatts-show     (xtag-atts  tag))))
       (string-append "\n<"name atts" />")))

(define (xtag-show t)                                                           ; #DOC #XMLGEN
   (cond
      ((xtag-empty? t)  (show-empty-tag t))
      ((xtag? t)        (show-tag-tag   t))
      ((string? t)      (translate-special-chars t))
      ((integer? t)     (show-item t))
      ((symbol?  t)     (show-item t))
      (else             (SWW "xtag-show" t))))


; =============================================================================
(print! 'xmlgen/)
; :file: END sts-q 2024-Aug
