; =============================================================================
; :file: random.sld               ; Random numbers, good enough to roll out the next tetris brick...

; =============================================================================
(define-library (stsq random)
  (export
    random-roll
    random-bool
    random-pop
    random-pop!
    random-shuffle
    random-parts-per
    __random/v)
  (import
    (scheme base)
    (scheme write)
    (scheme time)
    (tr7 extra)
    (tr7 foreigns)
    (stsq prelib)
    (stsq lib))
  (include "random.scm")
  (include "random-test.scm"))


; =============================================================================
; :file: END sts-q 2023-Sep

