; =============================================================================
; :file: random-test.scm

; =============================================================================
(define (__random/v)
  (section "__random/v")
  (lf) (for-each print_ (map random-roll (make-list 35 100)))
  (lf) (for-each print_ (map (lambda (x) (random-bool)) (make-list 35)))
  (let* ((n 10000) (s (sum (map random-roll (make-list n n))))) (say / n (div s n)) ok)
  (let* ((l (range-1 32))
          (ll (random-shuffle l)))
    (lf) (print l)
    (lf) (print ll)
    (lf) (print (sort ll)))
  (lff))


; =============================================================================
; :file: END sts-q 2023-Sep

