; =============================================================================
; :file: lib.scm  --  Pervasives.

; partial show-text path last goes say iter __lib ;
; =============================================================================
; :chapter: MATH
(define mod             modulo)                 ; #DOC #MATH
(define div             quotient)               ; #DOC #MATH

(define (neg n)         (- n))                  ; #DOC #MATH

(define (sgn n)                                 ; #DOC #MATH
   (cond
      ((zero? n)        0)
      ((positive? n)    1)
      (else            -1)))

(define (1+ n)          (+ n 1))                ; #DOC #MATH
(define (1- n)          (- n 1))                ; #DOC #MATH

(define (** n exp)                              ; #DOC #MATH                    # RTFM first: use expt, instead
   (cond
      ((= exp 0)        1)
      ((= exp 1)        n)
      (else             (* n (** n (1- exp))))))


(define (fac n)                                 ; #DOC #MATH
   (if (<= n 1)
      1
      (* n (fac (- n 1)))))

(define (fib n)                                 ; #DOC #MATH
   (if (<= n 1)
      n
      (+  (fib (- n 1))  (fib (- n 2)))))

(define (ack m n)                               ; #DOC #MATH Ackermann function
   (cond
      ((= m 0)   (+ n 1))
      ((= n 0)   (ack (- m 1) 1))
      (else      (ack (- m 1) (ack m (- n 1))))))

(define (collatz-chain n)                       ; #DOC #MATH
   (cond
      ((= n 1)          1)
      ((even? n)        (cons n (collatz-chain (div n 2))))
      (else             (cons n (collatz-chain (+ 1 (* n 3)))))))

(define (prime? n)                              ; #DOC #MATH Is n a prime number?
   (cond
      ((= n 1)          #f)
      ((= n 2)          #t)
      ((= n 3)          #t)
      ((= n 4)          #f)
      (else
         (letrec (
               (last (quotient n 2))
               (loop (lambda(x)
                        (cond
                           ((> x last)          #t)
                           ((= 0 (modulo n x))  #f)
                           (else                (loop (+ x 1)))
                           ))))
            (loop 2)))))

(define (sum l)         (apply + l))            ; #DOC #MATH Sum of list of numbers
(define (prod l)        (apply * l))            ; #DOC #MATH Product of list of numbers

(define (real->integer f)                       ; #DEPRECATED
   (let* ((s   (number->string (round f)))                                      ; today we have (exact (roundi))
          (len (string-length s)))
      (string->number (substring s 0 (- len 2)))))

(define (mean ns)                               ; #MATH #DOC Return mean of list of numbers
  (/ (sum ns) (length ns)))

; -----------------------------------------------------------------------------
; :section:     mutating
(define-syntax incr!                                                            ; #DOC #MATH #MUT
  (syntax-rules ()
    ((inrc! var)   (begin (set! var (+ var 1)) var))
    ((inrc! var n) (begin (set! var (+ var n)) var))))

(define-syntax decr!                                                            ; #DOC #MATH #MUT
  (syntax-rules ()
    ((decr! var)   (begin (set! var (- var 1)) var))
    ((decr! var n) (begin (set! var (- var n)) var))))


; =============================================================================
; chapter: CHAR
(define (char-at=? str i c)                     ; #CHAR #DOC Is chr at i eqv c?
   (eqv? c (string-ref str i)))


; =============================================================================
; :chapter: STRING
(define (string-blank? str)                               ; #DOC Is string empty or only whitespace (space|tab)
   (define len (string-length str))
   (let loop ((i 0))
      (cond
         ((>= i len)                                     #t)
         ((member (string-ref str i) '(#\space #\tab))   (loop (1+ i)))
         (else                                           #f))))

(define (string-head? str head)                                             ; #DOC Does string start with head?
   (let ((slen (string-length str))
         (hlen (string-length head))
         )
      (and (>= slen hlen) (string=? (substring str 0 hlen) head))))

(define (string-subseq? str seq)                                         ; #DOC Does string contain subseq seq?
   (let ((strlen (string-length str))
         (seqlen (string-length seq))
         )
      (cond
         ((string-head? str seq)        #t)
         ((<= strlen seqlen)            #f)
         (else                          (string-subseq? (substring str 1 strlen) seq)))))

(define (string-tail str start)                                  ; #DOC Return tail of string from start to end
   (let ((len  (string-length str)))
      (cond
         ((>= start len)        "")
         (else                  (substring str start len)))))

(define (string-find-char str c start)                        ; #DOC Return position of c | #f. Start at start.
   (define len (string-length str))
   (define (loop i)
      (cond
         ((>= i len)                    #f)
         ((eq? c (string-ref str i))    i)
         (else                          (loop (+ 1 i)))))
   (loop start))

(define (string-find-last-char str c start)     ; #DOC Return position of c | #f. #t -> start at end. Backwards
   (let* ((last    (1- (string-length str)))
          (start   (cond
                      ((eq? start #t)     last)
                      ((> start last)     last)
                      (else               start))))
      (letrec ((loop (lambda (i)
         (cond
            ((< i 0)                       #f)
            ((eq? c (string-ref str i))    i)
            (else                          (loop (1- i)))))))
      (loop start))))

(define (string-pad-left width str)                                             ; #DOC
  (let* ((len (string-length str))
         (pad (- width len)))
    (string-append (spaces pad) str)))

(define (string-pad-right width str)                                            ; #DOC
  (let* ((len (string-length str))
         (pad (- width len)))
    (string-append str (spaces pad))))

(define (string-pad-center width str)                                           ; #DOC
  (let* ((len (string-length str))
         (pad (- width len)))
    (string-append (spaces (div pad 2)) str (spaces (- width len (div pad 2))))))

(define (string-maximal width str)                                              ; #DOC
  (if (equal? "" str)
      ""
      (substring str 0 (min (string-length str) width))))

(define (string-?unquote str)                           ; #DOC unquote quoted string (ascii-34:  "..." --> ...)
  (let ((len (string-length str)))
    (if (and (char-at=? str 0 (integer->char 34)) (char-at=? str (- len 1) (integer->char 34)))
        (substring str 1 (- len 1))
        str)))


(define (chunks str)                                        ; #DOC split string at spaces (split line to words)
   (letrec ((len    (string-length str))
            (space? (lambda(pos)        (char=? #\space (string-ref str pos))))
            (end?   (lambda(pos)        (>= pos len)))
            (skip   (lambda(pos)        (if (or (end? pos) (not(space? pos)))  pos  (skip (1+ pos)))))
            (go     (lambda(pos)        (let ((p (skip pos)))  (loop p p))))
            (word   (lambda(start end)  (substring str start end)))
            (loop   (lambda(start pos)
               (cond
                  ((end? pos)                (if (= start pos) '() (list (word start pos))))
                  ((not(space? pos))         (loop start (1+ pos)))
                  (else                      (cons (word start pos)  (go pos)))))))
      (go 0)))


(define (chunks-1 c str)                                  ; #DOC split string at c (split string to CSV values)
   (letrec ((len        (string-length str))
            (isc?       (lambda(pos)            (char=? c (string-ref str pos))))
            (end?       (lambda(pos)            (>= pos len)))
            (chunk      (lambda(start end)      (substring str start end)))
            (loop       (lambda(start pos)
               (cond
                  ((end? pos)           (if (= start pos) '("")  (list (chunk start pos))))
                  ((not(isc? pos))      (loop start (1+ pos)))
                  (else                 (cons (chunk start pos)  (loop (1+ pos) (1+ pos))))
                  )))
                  )
      (loop 0 0)))


(define (spaces n)                                                              ; #DOC
  (make-string (max 0 n) #\space))


(define (words-inconcat sep words)                                              ; #DOC
  (letrec ((->string (lambda (x)
                       (cond ((symbol? x) (symbol->string x))
                             ((string? x) x)
                             (else "#f"))))
           (loop (lambda (ws)
                   (cond
                     ((null? ws) "")
                     ((= 1 (length ws)) (->string (car ws)))
                     (else (string-append (->string (car ws)) sep (loop (cdr ws))))))))
    (loop words)))

; =============================================================================
; :chapter: SYMBOL
(define enum-count 1000)

(define-syntax make-enums                                                   ; #DOC #SYM make enumerated symbols
  (syntax-rules ()
    ((_)   (begin ok))
    ((_ symbol ...)
         (for-each eval (flatten (map
                  (lambda(s) `(
                         (define ,s enum-count)
;                          (lf) (print ',s) (print "-|-") (print enum-count)
                         (incr! enum-count)))
                  '(symbol ...)))))))


; =============================================================================
; :chapter: LIST
(define first                   car)
(define second                  cadr)
(define third                   caddr)
(define fourth                  cadddr)
(define (fifth   x)             (car    (cddddr x)))
(define (sixth   x)             (cadr   (cddddr x)))
(define (seventh x)             (caddr  (cddddr x)))
(define (eighth  x)             (cadddr (cddddr x)))
(define (ninth   x)             (car    (cddddr (cddddr x))))
(define (tenth   x)             (cadr   (cddddr (cddddr x))))

(define (adjoin x l)                    ; #LIST #DOC
   (if (member x l)  l  (cons x l)))

(define (all? p xs)                    ; #DOC #LIST all predicated hold?
   (cond
      ((null? xs)       #t)
      ((p (first xs))   (all? p (cdr xs)))
      (else             #f)))

(define (collect n f . args)           ; #LIST #DOC Return list of n values got by applying f to args n times
   (let loop ((l '())  (n n))
      (cond
         ((<= n 0)     '())
         (else          (cons (apply f args) (loop l (1- n)))))))

(define (drop n l)                     ; #LIST #DOC Return l with the first n elts dropped or ().
  (cond
    ((null? l) '())
    ((positive? n) (drop (- n 1) (cdr l)))
    (else l)))

(define (drop-tail n lst)              ; #LIST #DOC Return lst without it's last n elements.
  (let ((len (length lst)))
    (take (- len n) lst)))

(define (filter f lst)                 ; #LIST #DOC Return filtered lst.
  (cond
    ((null? lst) '())
    ((f (car lst)) (cons (car lst) (filter f (cdr lst))))
    (else (filter f (cdr lst)))))

(define (find f lst)                   ; #LIST #DOC Return rest lst with first elt (f x) == true. #SEE member
  (cond
    ((null? lst) '())
    ((f (car lst)) lst)
    (else (find f (cdr lst)))))

(define (findpos f lst)                ; #LIST #DOC Return position of first elt of lst with (f x) == true.
  (letrec ((loop (lambda (p l)
                   (cond
                     ((null? l) #false)
                     ((f (car l)) p)
                     (else (loop (+ 1 p) (cdr l)))))))
    (loop 0 lst)))

(define (flatten ls)                            ; #DOC #LIST flatten list of lists non-recursively
  (assert 'flatten (all? list? ls))
  (cond
    ((null? ls) ())
    (else (append (car ls) (flatten (cdr ls))))))

(define (flat . args)                           ; #DOC #LIST return list with all args flattened recursively
   (letrec ((loop   (lambda(xs)
                       (cond
                          ((null? xs)           '())
                          ((list? (car xs))     (append (loop (car xs)) (loop (cdr xs))))
                          (else                 (cons   (car xs)        (loop (cdr xs))))
                          ))))
      (loop args)))

(define (join l x)   (append l (list x)))       ; #DOC #LIST

(define (last lst)   (first (reverse lst)))     ; #LIST #DOC

(define (list->table width lst)                 ; #DOC #LIST
  (letrec ((loop (lambda (l)
                   (cond
                     ((null? l) '())
                     ((< (length l) width) (list (append l (make-list (- width (length l)) #f))))
                     (else (cons (take width l) (loop (drop width l))))))))
    (loop lst)))

(define (pick elts l)                           ; #DOC #LIST pick elements of l into new list
   (assert 'pick (all? (partial > (length l)) elts))
   (map (partial list-ref l) elts))

(define (nub lst)                               ; #LIST #DOC nodups
   (if (null? lst)   '()   (adjoin (car lst) (nub(cdr lst)))))

(define (range-0 n)   (map 1- (range-1 n)))     ; #LIST #DOC Return 0..(n-1).

(define (range-1 n)                             ; #LIST #DOC Return 1..n.
   (letrec ((loop  (lambda(n)
            (if (<= n 0)
               '()
               (cons n (loop (1- n)))
               ))))
      (reverse (loop n))
      ))

(define (remove f lst)                          ; #LIST #DOC Return filtered lst.
  (cond
    ((null? lst) '())
    ((not (f (car lst))) (cons (car lst) (remove f (cdr lst))))
    (else (remove f (cdr lst)))))

(define (rive n lst)                   ; #LIST #DOC Return values:list of first n elts, list of remaining elts.
  (values (take n lst) (drop n lst)))

(define (separate f lst)               ; #LIST #DOC Return values: lst/test-passed, lst/test-failed.
  (letrec ((loop (lambda (l ts fs)
                   (cond
                     ((null? l) (values (reverse ts) (reverse fs)))
                     ((f (car l)) (loop (cdr l)
                                        (cons (car l) ts) fs))
                     (else (loop (cdr l)
                                 ts (cons (car l) fs)))))))
    (loop lst '() '())))

(define (slice p1 p2 lst)              ; #LIST #DOC Return slice of lst from ((p1 elt) == t) to ((p2 elt) ==t).
  (let* ((rest (find p1 lst))
         (pos (findpos p2 rest)))
    (if pos
        (take pos rest)
        rest)))

(define (sort lst)                     ; #LIST #DOC sort with lesser? #SEE lesser
  (if (null? lst)
      '()
      (let ((p (car lst)))
        (let-values (((lt gt) (separate (lambda (x) (lesser? x p)) (cdr lst))))
          (append (sort lt) (cons p (sort gt)))))))

(define (sort-with f lst)              ; #LIST #DOC sort with (lesser? (f elt)).  f is unary!
  (if (null? lst)
      '()
      (let* ((p (car lst))
             (f/p (f p))
             (f1 (lambda (x) (lesser? (f x) f/p))))
        (let-values (((lt gt) (separate f1 (cdr lst))))
          (append (sort-with f lt) (cons p (sort-with f gt)))))))

(define (take n l)                     ; #LIST #DOC Return first n elements of l or ().
  (cond
    ((null? l) '())
    ((positive? n) (cons (car l) (take (- n 1) (cdr l))))
    (else '())))

(define (take-tail n lst)              ; #LIST #DOC Return last n elts of lst
  (letrec ((len (length lst)))
    (cond
      ((negative? n) lst)
      ((<= len n)    lst)
      (else          (drop (- len n) lst)))))


(define (transpose l)           (apply map list l))     ; #DOC #LIST


;; ----------------------------------------------------------------------------
; :section:     alist: association list
;; k    key
;; v    value
;; kvp  key-value-pair: (list k v)
(define (alist-new)                             ; #LIST #ALIST #DOC Return empty alist
  '())

(define (alist-cons alist k v)                  ; #LIST #ALIST #DOC Return alist with (list k v) consed
  (cons (list k v) alist))

(define (alist-has-key? alist k)                ; #LIST #ALIST #DOC Return true |false
  (if (assoc k alist) #true #false))

(define (alist-remove alist k)                  ; #LIST #ALIST #DOC Rtn alist with first k removed
  (letrec ((loop (lambda (al)
                   (cond
                     ((null? al) '())
                     ((equal? k (caar al)) (cdr al))
                     (else (cons (car al) (loop (cdr al))))))))
    (loop alist)))

(define (alist-get alist k default)             ; #LIST #ALIST #DOC Return value of k |default.
  (let ((x (assoc k alist)))
    (if x (cadr x) default)))

(define (alist-get-key alist value default)     ; #DOC #LIST #ALIST Return key of value |default
   (define (loop al)
      (cond
         ((null? al)                    default)
         ((equal? value (cadar al))     (caar al))
         (else                          (loop (cdr al)))
         ))
   (loop alist))

(define (alist-update alist k v)           ; #LIST #ALIST #DOC Rtn alist with value of k updated to v |cons kvp
  (let ((al (alist-remove alist k)))
    (cons (list k v) al)))

(define-syntax alist-update!                    ; #LIST #ALIST #MUT #DOC (set! alist (alist-update alist k v))
  (syntax-rules ()
    ((_ alist k v) (set! alist (alist-update alist k v)))))

(define-syntax alist-exchange!            ; #LIST #ALIST #MUT #DOC Return old v of k, update alist with new kvp
  (syntax-rules ()
    ((_ alist k v)
     (let ((x (alist-get alist k #f)))
       (alist-update! alist k v)
       x))))

(define (alist-print width alist)               ; #LIST #ALIST #DOC Print alist.
  (let ((out (lambda (kvp)
               (lf) (print (car kvp)) (tab width) (print (cadr kvp)))))
    (for-each out (sort alist))
    (lff)))


; -----------------------------------------------------------------------------
; :section:     mutating
(define-syntax pop!                             ; #LIST #MUT #DOC Return car, set list to rest
  (syntax-rules ()
    ((pop! lst)
     (let ((x (car lst)))
       (set! lst (cdr lst))
       x))))

(define-syntax push!                            ; #LIST #MUT #DOC Set lst to x:lst, return new list
  (syntax-rules ()
    ((push! x lst)
     (begin (set! lst (cons x lst))
            lst))))


; =============================================================================
; :chapter: COMPARE
(define (lesser? a b)                           ; #DOC < for any (many) two types  #SEE sort sort-with
   (define (list-lesser? l1 l2)
      (cond
         ((and (null? l1) (null? l2))   #f)
         ((null? l1)                    #t)
         ((null? l2)                    #f)
         ((lesser? (car l1) (car l2))   #t)
         ((equal?  (car l1) (car l2))   (lesser? (cdr l1) (cdr l2)))
         (else                          #f)
         ))
   (define (type-lesser? t1 t2)
      ; sort like: booleans characters numbers strings symbols lists vectors anythingElse
      (cond
         ((boolean? t1)                 #t)
         ((boolean? t2)                 #f)
         ((char?    t1)                 #t)
         ((char?    t2)                 #f)
         ((number?  t1)                 #t)
         ((number?  t2)                 #f)
         ((string?  t1)                 #t)
         ((string?  t1)                 #t)
         ((symbol?  t1)                 #t)
         ((symbol?  t2)                 #f)
         ((list?    t2)                 #f)
         ((list?    t2)                 #f)
         ((vector?  t1)                 #t)
         (else                          #f)
         ))
   (cond
      ((and (number?  a) (number?  b))  (< a b))
      ((and (string?  a) (string?  b))  (string<? a b))
      ((and (symbol?  a) (symbol?  b))  (string<? (symbol->string a) (symbol->string b)))
      ((and (list?    a) (list?    b))  (list-lesser? a b))
      ((and (char?    a) (char?    b))  (char< a b))
      ((and (boolean? a) (boolean? b))  (and (not a) b))
      (else                             (type-lesser? a b))
      ))


; =============================================================================
; :chapter: SHOW
(define (show-int-00 n)                         ; #DOC Show integer: 01 09 12 ...
  (let ((s (number->string n)))
    (if (< n 10)
        (string-append "0" s)
        s)))

(define (show-int width n)                      ; #DOC Show integer right aligned to width.
  (string-pad-left width (number->string n)))

(define (show-bin width n)                      ; #DOC Show integer right aligned to width, base 2.
  (string-pad-left width (number->string n 2)))

(define (show-hex width n)                      ; #DOC Show integer right aligned to widths, base 16.
  (string-pad-left width (number->string n 16)))

(define (show-oct width n)                      ; #DOC Show integer right aligned to width, base 8.
  (string-pad-left width (number->string n 8)))

(define (show-real width frac r)                ; #DOC Show real number.
  (let* ((f1 (* r (expt 10 frac)))
         (i1 (exact (round f1)))
         (s (number->string i1))
         (len (string-length s))
         (mlen (- len frac)))
    (string-pad-left width (string-append (substring s 0 mlen) "." (string-tail s mlen)))))

(define (show-x width x)                        ; #DOC Show any value as string. Pad spaces up to width
  (cond
    ((and (number? x) (= x 0)) (show-int width x))
    ((and (number? x) (exact? x)) (show-int width x))
    ((and (number? x) (inexact? x)) (show-real width 3 x))
    ((string? x) (string-pad-right width x))
    ((symbol? x) (string-pad-right width (string-upcase (symbol->string x))))
    ((eqv? x #t) (string-pad-center width "#t"))
    ((eqv? x #f) (string-pad-center width "#f"))
    (else (parameterize ((current-output-port (open-output-string)))
            (display x)
            (get-output-string (current-output-port))))))

(define (show-text text)                                                        ; #DOC #TEXT concat lines
   (assert 'show-text (all? string? text))
   (cond
      ((null? text)     "")
      (else             (string-append (car text) "\n" (show-text (cdr text))))
      ))

(define (show-type x)                           ; #DOC #SHOW type of x
   (cond
      ((symbol? x)      'symbol)
      ((string? x)      'string)
      ((char?   x)      'char)
      ((integer x)      'integer)
      ((real?   x)      'real)
      ((number? x)      'number)
      ((null?   x)      'null)
      ((list?   x)      'list)
      (else             'no-idea-to-show-type)
      ))

; =============================================================================
; :chapter: SCAN
(define (scan-cents str)                        ; #DOC "-100,98" -> -10098. Return #f on failure.
  (guard (e
          (else #(begin
                  (lf) (print "<ERR:scan-cents: ") (print str) (print ">") (lf))
                #false))
         (let* ((sl (chunks-1 #\, str))
                (err (if (not (= 2 (string-length (second sl)))) (raise e)))
                (mant (read (open-input-string (first sl))))
                (frac (read (open-input-string (second sl)))))
           (cond
             ((not (= 2 (length sl))) #f)
             ((not (number? mant)) #f)
             ((not (number? frac)) #f)
             ((not (<= 0 frac 99)) #f)
             (else (+ (* mant 100) (if (< mant 0) (- 0 frac) frac)))))))


; =============================================================================
; :chapter: COMBINATORS
(define (&ps . ps)                              ; #DOC #COMBINATORS and-combine predicates: do all tests hold?
   (lambda (x)
      (cond
         ((null? ps)               #t)
         (((car ps) x)             ((apply &ps (cdr ps)) x))
         (else                     #f)
         )))

(define (or/ps . ps)                    ; #DOC #COMBINATORS or-combine predicates: does at least one test hold?
   (lambda (x)
      (cond
         ((null? ps)               #f)
         (((car ps) x)             #t)
         (else                     ((apply or/ps (cdr ps)) x))
         )))


(define (id . x) (apply values x))              ; #COMBINATORS #DOC Identity function.

(define (complement f)                          ; #COMBINATORS #DOC Return lambda: (not f)
  (lambda (x) (not (f x))))

(define-syntax compose                          ; #COMBINATORS #DOC Return lambda: (f (g (h . args)))  #SEE pipe
  (syntax-rules ()
    ((_) id)
    ((_ f) f)
    ((_ f g) (lambda args (f (apply g args))))
    ((_ f g h) (lambda args (f (g (apply h args)))))
    ((_ f g h i) (lambda args (f (g (h (apply i args))))))
    ((_ f g h i j) (lambda args (f (g (h (i (apply j args)))))))))

(define (flip f)                                ; #DOC #COMBINATORS
  (lambda (x y) (f y x)))

(define (pipe/v f . rest)                       ; #COMBINATORS #DOC Return lambda: (pipe f g h) -->  (h (g (f x)))
  (if (null? rest)
      f
      (let ((g.h (apply pipe/v rest)))
        (lambda x
          (let-values ((x (apply f x)))
            (apply g.h x))))))

(define-syntax pipe              ; #COMBINATORS #DOC Return lambda: (pipe f g h) --> (h (g (f x))) #SEE compose
  (syntax-rules ()
    ((_) id)
    ((_ f) f)
    ((_ f g) (lambda args (g (apply f args))))
    ((_ f g h) (lambda args (h (g (apply f args)))))
    ((_ f g h i) (lambda args (i (h (g (apply f args))))))
    ((_ f g h i j) (lambda args (j (i (h (g (apply f args)))))))))


; =============================================================================
; :chapter: GO
(define-syntax go                               ; #DOC #SAY Print form, evaluate and print results
  (syntax-rules ()
    ((_ frm) (begin
               (lf) (dot-as-std) (print 'frm)
               (lf) (dot-as-text)
               (guard (e
                       (else (if (error-object? e)
                                 (print-err-object 'go e)
                                 (say / "############  Unkown error."))))
                      (let-values ((all (eval 'frm)))
                        (for-each println all)))
               (lff)))))
     
(define-syntax goes                             ; #DOC #SAY (goes 'from-somewhere (+ 3 4) (* 3 4) ...)
   (syntax-rules ()
      ((_ from q ...)
         (begin
            (let ((f from))
            (begin
               (lff) (dot-as-dbg) (print "-------  goes " ) (print f) (print ": ") (print 'q)
               (dot-as-text) (flush-output-port) (lf)
               (print (eval 'q (current-environment)))
               ) ... (lff))))
               ))

; =============================================================================
; :chapter: PATH
(define HOME (string-append (string-?unquote (get-environment-variable "HOME")) "/"))
(define PWD (string-append (string-?unquote (get-environment-variable "PWD")) "/"))

(define (path-home . path)                      ; #DOC #PATH
   (apply string-append HOME   (values path)))

(define (path-pwd-2 . path)                     ; #DOC #PATH
   (apply string-append PWD    (values path)))

(define (path-pwd . path)                       ; #DOC #PATH
  (letrec ((loop (lambda (ps)
                   (cond
                     ((null? ps) "")
                     (else (string-append (car ps) (loop (cdr ps))))))))
    (string-append PWD (loop path))))

(define (path-file-name path)                   ; #DOC #PATH return 'file.ext'
   (let* ((pos  (string-find-last-char path #\/ #t)))
      (if pos
         (string-tail path (1+ pos))
         path)))


; =============================================================================
; :chapter: FILES
(define (file-read name)                        ; #DOC #FILE Return scheme forms read from file. #FIXME
  (letrec ((errmess (lambda ()
                      (lff) (dot-as-err)
                      (print "############  file-read: read error: ") (print name)
                      (dot-as-text) (lf)))
           (port (open-input-file name))
           (loop (lambda ()
                   (let ((form (guard
                                (e (else (errmess)))
                                (read port))))
                     (cond
                       ((eof-object? form) '())
                       (else (cons form (loop)))))))
           (forms (loop)))
    (close-input-port port)
    forms))

(define (file-read-lines name)                  ; #DOC #FILE Return list of lines of file, without NLs.
  (define (readlines)
    (let ((l (read-line)))
      (cond
        ((eof-object? l) '())
        (else (cons l (readlines))))))
  (with-input-from-file name readlines))

(define (file-write-lines name lines)           ; #DOC #FILE Write line to file. #FIXME append last newline.
  (define (loop ls)
    (cond
      ((null? ls) ok)
      (else (write-string (car ls)) (write-char #\newline) (loop (cdr ls)))))
  (with-output-to-file name (lambda () (loop lines))))


(define file-touch-times (alist-new))           ; #FILE #SEE (__files/v)

(define (file-touched? name)                    ; #DOC #FILE Return true if file was touched since last call.
  (let* ((tt (file-touch-time name))
         (last (alist-exchange! file-touch-times name tt)))
    (cond
      ((not (and tt last))   #f)
      ((< last tt)           (alist-update! file-touch-times name tt) #t)
      (else                  #f))))


(define (file-load name)                        ; #DOC #FILE read and evaluate all scheme forms of file. #FIXME
  (define success #t)
  (let* ((errmess (lambda (e f)
                    (lff) (dot-as-err)
                    (print "############  ") (print " file-load: eval form failed: ")
                    (lf) (print name)
                    (lf) (print f)
                    (lf) (if (error-object? e) (print-err-object e))
                    (dot-as-text) (lf)
                    (set! success #f)))
         (exec (lambda (f)
                 (guard (e (else (errmess e f)))
                        (eval f)))))
    (for-each exec (file-read name))
    success))


; =============================================================================
; :chapter: SYSTEM
(define (msleep micro-secs)                     ; #SYS #DOC sleep microseconds.
   (let ((secs   (div micro-secs 1000))
         (nanos  (* 1000000 (mod micro-secs 1000))))
      (nanosleep secs nanos)))


(define (time-values-list)                      ; #SYS #DOC Return '(hh mm dd).
   (let* ((utc+   1)
          (sec    (exact (round (current-second))))
          (dsec   (remainder sec 86400))
          (h      (+ utc+   (quotient  dsec 3600)))
          (m      (quotient (remainder dsec 3600) 60))
          (s      (remainder dsec 60))
          )
      (list h m s)))


(define (now)                                   ; #SYS #DOC Return "hh-mm-dd".   #SEE (current-caldate)
   (let ((vs (time-values-list)))
      (string-append (show-int-00 (first vs)) ":" (show-int-00 (second vs)) ":" (show-int-00 (third vs)))))

(define (cmd . args)                            ; #SYS #DOC Return (words-inconcat " " args)
  (words-inconcat " " args))

(define (cmd/p . args)                          ; #SYS #DOC Print and return (cmd args).
  (let ((cmd (words-inconcat " " args)))
    (lf) (dot-as-dbg) (print cmd)
    (lf) (dot-as-text)
    (flush-output-port)
    cmd))

(define (system/read-lines . args)              ; #DOC #SYS return(values cmd-output cmd-exit-code) via tempfile
   (let* ((tempfile      "work/tempfile.txt")
          (c             (apply cmd (values args)))
          (c             (string-append c " >" tempfile)))
;       (lf) (print c) (lf)
      (let ((res (system c)))
      (values (file-read-lines tempfile) res))))


; =============================================================================
(print! 'lib/)
; :file: END sts-q 2023-Sep
