;;; ===========================================================================
;;; :file: tt.scm  --  Simple table functions.


;;; ===========================================================================
(define (rec-addcol f)                          ; #TT #DOC Return f: rec -> (rec ++ [f rec]).
  (lambda (rec) (join rec (odds-on f rec))))

(define (rec-has-at? fpos val rec)              ; #TT #DOC Return bool: val == (fpos rec)
  (equal? val (fpos rec)))

(define (tt-colsum fpos tt)                     ; #TT #DOC Return sum of tt-column.
  (odds-on sum (map fpos tt)))

(define-syntax tt-map!                          ; #TT #DOC tt := (map f tt)
  (syntax-rules ()
    ((_ tt f) (set! tt (map f tt)))))

(define (tt-map-col f col tt)                   ; #DOC #TT apply f to all values of col
   (define (g rec)
      (assert 'tt-map-col (> (length rec) col))
      (let-values (((head tail)   (rive col rec)))
         (append  head   (cons  (f (car tail))  (cdr tail)))))
   (map g tt))

(define (tt-only fpos test tt)                  ; #TT #DOC Filter tt: (test (fpos rec)) == true
  (filter (lambda (rec)
            (test (fpos rec)))
          tt))

(define (tt-print width name tt)                ; #TT #DOC Print tt.
  (letrec ((print-value (lambda (v) (print (show-x width v)) (spc)))
           (print-line (lambda (line) (lf) (for-each print-value line))))
    (section name)
    (for-each print-line tt)
    (lff)))


(print! 'tt/)
;;; ===========================================================================
;;; :file: END sts-q 2023-Nov
