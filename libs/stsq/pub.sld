;;; ===========================================================================
;;; :file: pub.sld  --  Building html with help of xmlgen.

; =============================================================================
(define-library (stsq pub)
   (export
      xprint
      xhtml
      xhead xbody
      xtitle xmeta xstyle
      xh1 xh2 xbr
      xp
      xa
      xheadline xchapter xfoot xcode
      __pub/v)

   (import
      (scheme base)
      (tr7 foreigns)
      (scheme eval)
      (tr7 environment)
      (stsq prelib)
      (stsq lib)
      (stsq xmlgen))

   (include "pub.scm")
   (include "pub-test.scm"))


;;;  ==========================================================================
;;; :file: END sts-q 2024-Aug
