; =============================================================================
; :file:        caldate.scm             Limited calender date calculations.

; =============================================================================
; type year    == integer
; type month   == integer
; type day     == integer
; type daynum  == integer
; type caldate == (year month day)

(define first-valid-year 1900)
(define last-valid-year 2100)
(define first-valid-daynum 0)
(define last-valid-daynum 73413)
(define month-lengths '(0 31 28 31 30 31 30 31 31 30 31 30 31))       ; index Jan with 1

(define (valid-daynum? dn) (<= first-valid-daynum dn last-valid-daynum))

; -----------------------------------------------------------------------------
; :section:     read from string and show to string
(define (strings->caldate year month day)
  (let ((y (string->number year))                                ; string->number returns #f on invalid number
         (m (string->number month))
         (d (string->number day)))
    (if (and y m d (valid-caldate? y m d))
        (list y m d)
        (err "strings->caldate: not a valid date." (list year month day)))))

(define (show-caldate-iso caldate)                      ; #DOC show caldate ISO format: "yyyy-mm-dd"
  (string-append
   (show-int 4 (first caldate))
   "-" (show-int-00 (second caldate))
   "-" (show-int-00 (third caldate))))

(define show-caldate show-caldate-iso)                  ; #DOC show caldate std iso format

(define (read-caldate-iso yyyy-mm-dd)                   ; #DOC read from string yyyy-mm-dd -> caldate
  (let ((y (substring yyyy-mm-dd 0 4))
          (m (substring yyyy-mm-dd 5 7))
          (d (substring yyyy-mm-dd 8 10)))
    (strings->caldate y m d)))

(define (read-caldate-de dd.mm.yyyy)                    ; #DOC read from string dd.mm.yyyy -> caldate
  (let ((y (substring dd.mm.yyyy 6 10))
          (m (substring dd.mm.yyyy 3 5))
          (d (substring dd.mm.yyyy 0 2)))
    (strings->caldate y m d)))


; -----------------------------------------------------------------------------
; :section:     convert caldate <-> daynum
(define (caldate->daynum cd)                            ; #DOC convert daynum -> caldate
  (define-values (y m d) (apply values cd))
  (let* ((leapyears (div (- y 1 first-valid-year) 4))
          (mlens (map (lambda (m) (month-length y m)) '(0 1 2 3 4 5 6 7 8 9 10 11 12)))
          (ydays (+ leapyears (* 365 (- y first-valid-year))))
          (mdays (sum (take m mlens)))                                   ; don't count current month
          (days d))
    (+ ydays mdays days -1)))

(define (daynum->caldate dn)                            ; #DOC convert caldate -> daynum
  (let* ((y (+ first-valid-year (div (* (+ dn 1) 100) 36525)))
          (mlens (cons 31 (cons (month-length y 2) '(31 30 31 30 31 31 30 31 30 31))))
          (r (- dn (caldate->daynum (list y 1 1)) -1)))
    (define (collect m r)
      (cond ((<= r (first mlens)) (list m r))
            (else (collect (+ 1 m) (- r (pop! mlens))))))
    (cons y (collect 1 r))))


; -----------------------------------------------------------------------------
; :section:     calculating with calender dates
(define (valid-caldate? year month day)                 ; #DOC Is this a valid date?
  (and
   (<= first-valid-year year last-valid-year)
   (<= 1 month 12)
   (<= 1 day (month-length year month))))

(define (leapyear? year)                                ; #DOC Is year a leapyear?
  (cond ((= 0 (mod year 400)) #t)                     ; keep this order!
        ((= 0 (mod year 100)) #f)
        ((= 0 (mod year 4)) #t)
        (else #f)))

(define (month-length year month)                       ; #DOC return length of month
  (cond ((and (= month 2) (leapyear? year)) 29)
        (else (list-ref month-lengths month))))

(define (caldate-diff cd1 cd2)                          ; #DOC return difference of dates in days
  (- (caldate->daynum cd1) (caldate->daynum cd2)))

(define (caldate-add-days caldate days)                 ; #DOC return caldate + days
  (daynum->caldate (+ days (caldate->daynum caldate))))

(define (current-daynum)                                ; #DOC return todays daynum
  (let ((d/utime (div (exact (round (current-second))) 86400))
        (daynum/epoch (caldate->daynum '(1970 1 1))))
    (+ d/utime daynum/epoch)))

(define (current-caldate)                               ; #DOC return todays caldate
  (daynum->caldate (current-daynum)))

(define (today)                                         ; #DOC return string with todays date
  (show-caldate-iso (current-caldate)))

(define (today-now)                                     ; #DOC return string with current weekday, date and time
  (string-append (show-weekday (daynum->weekday (current-daynum))) "_" (today) "_" (now)))

; =============================================================================
; :chapter: WEEKDAY
(define (show-weekday wd)
  (case wd
    ((0) "Sun")
    ((1) "Mon")
    ((2) "Tue")
    ((3) "Wed")
    ((4) "Thu")
    ((5) "Fri")
    ((6) "Sat")
    (else #f)))

(define (daynum->weekday dn)
  (let ((some-sundays-daynum -1))
    (mod (- dn some-sundays-daynum) 7)))


; =============================================================================
(print! 'caldate/)
; :file: END sts-q 2023-Sep

