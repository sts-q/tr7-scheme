;;; ===========================================================================
;;; :file: bit-test.scm

(define (__bit)
  (test 'bit-shl (bit-shl 2 0) 2)
  (test 'bit-shl (bit-shl 2 1) 4)
  (test 'bit-shl (bit-shl 2 2) 8)
  ;; 
  (test 'bit-asr (bit-asr 2 0) 2)
  (test 'bit-asr (bit-asr 2 1) 1)
  (test 'bit-asr (bit-asr 2 2) 0)
  (test 'bit-asr (bit-asr -2 0) -2)
  (test 'bit-asr (bit-asr -2 1) -1)
  (test 'bit-asr (bit-asr -2 2) -1)
  ;; 
  (test 'bit-members (bit-members 0) '())
  (test 'bit-members (bit-members 1) '(0))
  (test 'bit-members (bit-members 7) '(2 1 0))
  (test 'bit-members (bit-members 14) '(3 2 1))
  (test 'bit-members (length (bit-members -1)) 32)
  ;;  
  (test 'bit-and (bit-and) -1)
  (test 'bit-and (bit-and 1 3 5 7 9) 1)
  (test 'bit-and (bit-and 3 5) 1)
  (test 'bit-and (bit-and 2 4) 0)
  ;; 
  (test 'bit-or (bit-or) 0)
  (test 'bit-or (bit-or 1) 1)
  (test 'bit-or (bit-or 1 2) 3)
  (test 'bit-or (bit-or 1 2 4 8 16) 31)
  ;; 
)


;;; ===========================================================================
;;; :file: END sts-q 2023-Sep

