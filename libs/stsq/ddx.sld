; =============================================================================
; :file: ddx.sld

; =============================================================================
(define-library (stsq ddx)
  (export
    *doctab*
    dda
    ddw
    ddt
    ddg
    ddws
    ddls
    ddts
    trans
    help
    hello!
    hello?
    __ddx/v)

  (import
    (scheme base)
    (scheme file)
    (scheme char)
    (scheme eval)
    (tr7 environment)
    (tr7 foreigns)
    (tr7 misc)
    (stsq prelib)
    (stsq lib)
    (stsq tt))

  (include
     "doctab.scm"
     "ddx.scm")

  (begin #t))

   
; =============================================================================
; :file: END sts-q 2023-Oct

