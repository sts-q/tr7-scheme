; =============================================================================
; :file: lib.sld               ; Pervasives! Yes!

; partial today now last ;

(define-library (stsq lib)
  (export ; :chapter:
          HOME PWD
          incr! decr!
          mod div neg sgn 1+ 1-        ; MATH
          **
          fib fac ack
          prime?
          collatz-chain
          sum prod
          mean
          real->integer
          char-at=?                    ; CHAR
          string-blank?                ; STRING
          string-head?
          string-subseq?
          string-tail
          string-find-char
          string-find-last-char
          string-pad-left
          string-pad-right
          string-pad-center
          string-maximal
          string-?unquote
          enum-count make-enums
          ;;                           text words
          words-inconcat
          chunks                       ; LIST
          chunks-1
          spaces
          first second third fourth fifth sixth seventh eighth ninth tenth
          all?
          adjoin
          collect
          join
          list->table
          last
          flatten flat
          nub                       ; (lst)                 remove duplicates using 'adjoin/member'
          pick
          range-0 range-1
          remove
          sort
          take drop take-tail drop-tail
          filter separate rive
          find findpos
          slice
          sort-with
          transpose
          ;; ------------  alist
          alist-new
          alist-cons
          alist-has-key?
          alist-remove
          alist-get
          alist-get-key
          alist-update
          alist-update!
          alist-exchange!
          alist-print
          ;; ------------  list update
          pop! push!
          show-bin show-hex
          show-int-00 show-int         ; show
          show-x show-real
          show-text
          show-type
          scan-cents
          id compose complement flip pipe/v pipe &ps or/ps
          go goes
          path-home path-pwd           ; PATH
          path-file-name
          file-read                    ; FILE
          file-read-lines
          file-write-lines
          file-touched?
          file-load
          msleep                       ; SYSTEM
          time-values-list
          now
          cmd
          cmd/p
          system/read-lines
          __alist/v
          __files/v)

  (import ; :chapter:
          (scheme base)
          (scheme read)
          (scheme write)               ; show-x
          (scheme char)
          (scheme file)
          (scheme eval)
          (scheme cxr)
          (scheme time)
          (scheme process-context)
          (tr7 debug)
          (tr7 environment)
          (tr7 foreigns)
          (stsq prelib))

  (include "lib.scm")
  (include "lib-test.scm")

  (begin
     (__lib)
     ))


; =============================================================================
; :file: END sts-q 2023-Sep

