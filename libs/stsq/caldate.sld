; =============================================================================
; :file: caldate.sld               ; A simple wrapper around (scheme hash).

; =============================================================================
(define-library (stsq caldate)
  (export ; :chapter:
          show-caldate-iso                                  ; "yyyy-mm-dd"
          show-caldate                                      ; "yyyy-mm-dd", alias for ..-iso
          read-caldate-iso                                  ; "yyyy-mm-dd"
          read-caldate-de                                   ; "dd.mm.yyyy"
          caldate->daynum                                   ; (yyyy mm dd) -> integer
          daynum->caldate                                   ; integer -> (yyyy mm dd)
          valid-caldate?                                    ;
          leapyear?                                         ;
          month-length                                      ;
          caldate-diff                                      ;
          caldate-add-days                                  ;
          current-daynum                                    ; integer
          current-caldate                                   ; (year month day)
          today                                             ; "yyyy-mm-dd"
          today-now                                         ; "yyyy-mm-dd_hh:mm:ss"
          __caldate/v)                                      ; verbose test
  (import ; :chapter:
          (scheme base)
          (scheme time)
          (stsq prelib)
          (stsq lib))
  (include "caldate.scm")
  (include "caldate-test.scm")
  (begin (__caldate)))


; =============================================================================
; :file: END sts-q 2023-Sep

