(define-library (scheme division)
  (export
;;; Exported:
   ceiling/ ceiling-quotient ceiling-remainder
   floor/ floor-quotient floor-remainder
   truncate/ truncate-quotient truncate-remainder
   round/ round-quotient round-remainder
   euclidean/ euclidean-quotient euclidean-remainder
   balanced/ balanced-quotient balanced-remainder
  )
  (begin (include "division.scm"))
)


