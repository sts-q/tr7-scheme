;;; ===========================================================================
;;; :file: scratch.scm          Scratchfile.

(define (scratch)                      ; :chapter:
  (section "SCRATCH")
  (go (bit-count 12))
  ;; 
)
  

;;; ===========================================================================
(define (main)                          ; :chapter:
  (section "MAIN")
  (go (+ 3 4))
  (section "MAIN done")
  (notify-if-some-tests-failed))


;;; ===========================================================================
(print "scratch/") 
;;; :file: END sts-q 2023-Oct
