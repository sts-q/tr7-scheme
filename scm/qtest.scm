; =============================================================================
; :file: qtest.scm      ; Quick repository test.

(import
  (stsq prelib)
  (stsq lib)
  (stsq bit)
  (stsq random)
  (stsq caldate)
  (stsq ddx)
  (stsq repl)           ; for ./seven qtest --cmd repl
)


; =============================================================================
; :chapter: QTEST
(define (__qtest)
  (section "__qtest")
  (ddws second)
  (say /_ (fib 10) ~)
  (say /_ (fac 10) ~)
  (say /_ (bit-members 10) ~)
  (say /_ (let ((n 10000)) (list n (div (sum (collect n random-roll n)) n))) ~)
  (say /_ (caldate-diff '(2024 12 24) (current-caldate)) " days until Noël 24.")
  (say //))


(print! 'qtest/)
; =============================================================================
