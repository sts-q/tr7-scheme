; =============================================================================
; :file: main.scm
(display '<main:) (flush-output-port)

(import
  (tr7 foreigns)
  (stsq prelib)
  (stsq lib)
  (stsq bit)
  (stsq random)
  (stsq caldate)
  (stsq csv)
  (stsq ddx)
  (stsq repl))

(include "scm/scratch.scm")
 

(print ':main>)  
(lf)


;;; ===========================================================================
;;; :file: END sts-q 2023-Sep


