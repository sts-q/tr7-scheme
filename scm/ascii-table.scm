; =============================================================================
; :file: ascii-table.scm        Print ascii table.

; =============================================================================
(import
  (stsq prelib)
  (stsq lib))


; =============================================================================
(section "ASCII Table")

(let ((first 33)
      (last 126)
      (out (lambda (i) (print (show-int 5 i)) (spc) (print (integer->char i))))
      (?lf (lambda (i) (when (zero? (mod i 16)) (lf))))
      (?md (lambda (i) ; ~markdown~
             (case i
               ((48) (dot-as-dbg))
               ((58) (dot-as-text))
               ((65) (dot-as-prompt))
               ((91) (dot-as-text))
               ((97) (dot-as-comment))
               ((123) (dot-as-text))
               (else #t)))))
  (let loop ((i first)) (when (<= i last) (?md i) (out i) (?lf i) (loop (1+ i)))))

(lff)


; =============================================================================
; :file: END sts-q 2023-Sep

