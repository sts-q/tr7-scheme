;;; ============================================================================
;;; :file: test-more.scm  --  More tests about this all.

(import
  (stsq prelib)
  (stsq lib)

  (stsq bit)
  (stsq csv)
  (stsq ddx)

  (stsq caldate)
  (stsq repl)
  (stsq random)
  (stsq xmlgen)
  (stsq pub))


;;; ============================================================================
;;; :chapter: MAIN
(define (test-more)
   (section "test-more run")
   (__caldate/v)
   (__repl/v)
   (__random/v)
   (__alist/v)
   (__files/v)
   (__xmlgen/v)
   (__pub/v)
   (__ddx/v)
   (__say/v)
  (section "Results of silent tests")
  (say /_ tests-counter "of" tests-count "silent tests done.")
  (notify-if-some-tests-failed)
  (section "test-more done"))

(test-more)
;;; ============================================================================
;;; :file: END sts-q 2023-Oct

