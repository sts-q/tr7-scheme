
## TR7

TR7 is the Tiny R7RS-Small Scheme interpreter for embedding in C programs.

TR7 is written by (c) Jose Bollo and others.

https://gitlab.com/jobol/tr7

https://small.r7rs.org/attachment/r7rs.pdf


## Here

This repo holds an employment of TR7 by sts-q.

If you are new to TR7, please start with the original repository.


### System requirements here

 * Linux

 * tcc or gcc or clang


### Usage now here:

        ./now                          # Print now usage.

        ./now -mo                      # Compile new seven binary.
        ./now qtest                    # Run quick test.
        ./now repl                     # Start scheme repl.

        ./seven                         # Print help message.
        ./seven -v                      # Print version info.
        ./seven -                       # Start inbuild C-repl.

        ./seven ascii-table             # Print ascii table.
        ./seven test-more               # Run more and verbose tests.


## License

        Copyright 2021-2023 the TR7 team
        Copyright 2023 sts-q

        Zero-Clause BSD

        Permission to use, copy, modify, and/or distribute this software for any
        purpose with or without fee is hereby granted.

        THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
        WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
        MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
        ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
        WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
        ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
        OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


## Contact

Ping [ sts-q ]( https://sts-q.codeberg.page/ ) at libera.chat #oberon.

This repository was first published at codeberg at 2023-09-18.

