// ============================================================================
// :file: seven.c               Wrapper for TR7

// shell feature version //
// ----------------------------------------------------------------------------
// :section:    include
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>
#include <ctype.h>
#include <time.h>
#include <wchar.h>
#include <locale.h>
#include <signal.h>


#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/stat.h>


#include "tr7.h"


// ----------------------------------------------------------------------------
// :section:    config
#define CONF_VERSION     VERSION
#define CONF_VERSIONSTR "Seven --  TR7 scheme build with: " CONF_VERSION" "D_COMPI" "D_OSNAME" "D_COMPTIME "\n"
#define CONF_PROMPT     "seven-C-repl> "                // repl prompt

// #define CONF_PATH       D_PWD"/scm"                     // load
// #define CONF_LIBPATH    D_PWD"/libs"                    // import
#define CONF_PATH       "scm"                     // load
#define CONF_LIBPATH    "libs"                    // import
#define CONF_INCPATH    ""                              // include files
#define CONF_EXTPATH    ""                              // load-extension

#define CONF_CMD_C_REPL      "-"
#define CONF_CMD_SCM_REPL    "+"
#define CONF_CMD_FUN         "--cmd"
#define CONF_CMD_EXPR        "-c"
#define CONF_CMD_PIPE        "--"


// ----------------------------------------------------------------------------
// :section:    lib
void print_helpmess (void)
{
            printf ("=======  seven -- A TR7 repl wrapper.");
            printf ("\n    seven                              # Print this text.");
            printf ("\n    seven -h                           # Print this text.");
            printf ("\n    seven --help                       # Print this text.");
            printf ("\n    seven -v                           # Print version info.");
            printf ("\n    seven [<file>]*                    # Load files.");
            printf ("\n    seven [<file>]*  --cmd <cmd>       # Load files, then exec (<cmd>).");
            printf ("\n    seven [<file>]*  -c '<expr>'       # Load files, then exec <expr>.");
            printf ("\n    seven [<file>]*  -                 # Load files, then start C-repl.");
            printf ("\n    seven [<file>]+  +                 # Load files, then start SCM-repl.");
            printf ("\n    seven [<file>]*  --                # Load files, then read form stdin (ROADWORK).");
            printf ("\n\n");
}

void import_buildin_libs ( tr7_engine_t tsc)
{
        // This file, seven.c, needs: display, exit.
        tr7_import_lib ( tsc, "scheme/base");
        tr7_import_lib ( tsc, "scheme/write");
        tr7_import_lib ( tsc, "scheme/process-context");

        // All libraries do their own imports.
        // All following imports are done to make their exports available 
        // for scm-repl and standalone programs
        // without an extra (import (lib name)).
        // All imports done here are build into tr7.c .
        tr7_import_lib ( tsc, "scheme/char");
        tr7_import_lib ( tsc, "scheme/cxr");
        tr7_import_lib ( tsc, "scheme/eval");
        tr7_import_lib ( tsc, "scheme/file");
        tr7_import_lib ( tsc, "scheme/inexact");
        tr7_import_lib ( tsc, "scheme/load");
        tr7_import_lib ( tsc, "scheme/read");
        tr7_import_lib ( tsc, "scheme/repl");
        tr7_import_lib ( tsc, "scheme/time");
        tr7_import_lib ( tsc, "scheme/write");

        tr7_import_lib ( tsc, "tr7/extra");
        tr7_import_lib ( tsc, "tr7/foreigns");    // ??? Needs to be imported by some.scm ?
        tr7_import_lib ( tsc, "tr7/environment");
        tr7_import_lib ( tsc, "tr7/debug");
        tr7_import_lib ( tsc, "tr7/gc");
        tr7_import_lib ( tsc, "tr7/misc");
        tr7_import_lib ( tsc, "tr7/trace");
}


// ============================================================================
// :chapter: FOREIGN FUNCTIONS
static tr7_C_return_t   system_cxp (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int status = system (tr7_string_buffer (args[0]));
        return tr7_C_return_single (tsc, status == 0 ? TR7_TRUE : TR7_FALSE);
}

static tr7_C_return_t   process_cxp (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        FILE *fp;
        tr7_t fds[4];
        fds[0] = TR7_TRUE;
        fds[1] = TR7_TRUE;
        fds[2] = TR7_TRUE;
        fds[3] = tr7_from_int (tsc, 0);

        fp = popen  (tr7_string_buffer (args[0]), "r");
        if (fp == NULL) {
            fds[0] = TR7_FALSE;
        } else {
//            int fx = (int) (*fp);
//            tr7_t px = TR7_AS_PORT (5);
            fds[0] = tr7_from_int (tsc, 1700);
        }

        return tr7_C_return_values (tsc, 4, fds);
}


static tr7_C_return_t   myadd_cxp   (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int arg1   = tr7_to_double( args[0] );
        int arg2   = tr7_to_double( args[1] );
        int res    = arg1 + arg2;
        tr7_t tres = tr7_from_int  (tsc, res);
        return tr7_C_return_single (tsc, tres);
}


// ----------------------------------------------------------------------------
/* Get terminal size. */
// not working with ledit or rlwrap
short databuffer[2];

static tr7_C_return_t  xterm_width (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        /* int lines = 0; */
        int cols  = 0;
        ioctl(0, TIOCGWINSZ, databuffer);
        /* lines = databuffer[0]; */
        cols  = databuffer[1];
        tr7_t res = tr7_from_int   (tsc, cols);
        return tr7_C_return_single (tsc, res );
}

static tr7_C_return_t  xterm_height (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int lines = 0;
        /* int cols  = 0; */
        ioctl(0, TIOCGWINSZ, databuffer);
        lines = databuffer[0];
        /* cols  = databuffer[1]; */
        tr7_t res = tr7_from_int   (tsc, lines);
        return tr7_C_return_single (tsc, res );
}


// ----------------------------------------------------------------------------
// :section:    nanosleep
struct timespec sleep_req;
struct timespec sleep_rem;
int nanosleep( const struct timespec *req, struct timespec *rem);
int nanosleep_fun( int sec, int nsec ) {
        sleep_req.tv_sec  = sec;
        sleep_req.tv_nsec = nsec;
        int res = nanosleep( &sleep_req, &sleep_rem );
        return res;
}

static tr7_C_return_t   nanosleep_cxp  (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int sec  = tr7_to_double (args[0]);
        int nsec = tr7_to_double (args[1]);
        int res  = nanosleep_fun (sec, nsec );
        tr7_t rtn = tr7_from_int (tsc, res);
        return tr7_C_return_single (tsc, rtn);
}

// ----------------------------------------------------------------------------
// :section:    file-touch-time
struct stat statbuffer;
int stat (const char *pathname, struct stat *statbuf);

int cFileTouchTime( const char *name ) {
        int res = stat( name, &statbuffer );
        if (res == 0) {
                /* c last change */
                /* a last access */
                /* m last modification */
                int time = statbuffer.st_mtime;
                /* int time = statbuffer.st_atime; */
                /* int time = statbuffer.st_mtime; */
                return time;
        } else {
                return -1;
        }
}

static tr7_C_return_t   file_touch_time_cxp  (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int    time = cFileTouchTime (tr7_string_buffer (args[0]));
        tr7_t  rtn  = time != -1 ? tr7_from_int (tsc, time) : TR7_FALSE;
        return tr7_C_return_single (tsc, rtn );
}


// ----------------------------------------------------------------------------
// :section:    bitwise
static tr7_C_return_t   bit_shl_cxp  (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int arg1   = tr7_to_double( args[0] );
        int arg2   = tr7_to_double( args[1] );
        int res    = arg1 << arg2;
        tr7_t tres = tr7_from_int  (tsc, res);
        return tr7_C_return_single (tsc, tres);
}

static tr7_C_return_t   bit_asr_cxp  (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int arg1   = tr7_to_double( args[0] );
        int arg2   = tr7_to_double( args[1] );
        int res    = arg1 >> arg2;
        tr7_t tres = tr7_from_int  (tsc, res);
        return tr7_C_return_single (tsc, tres);
}

static tr7_C_return_t   bit_and_cxp  (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int res = -1;
        for (int i = 0; i < nargs; i++)  {  
            int v = tr7_to_double (args[i]);
            res   = res & v;
        }
        tr7_t tres = tr7_from_int  (tsc, res);
        return tr7_C_return_single (tsc, tres);
}

static tr7_C_return_t   bit_or_cxp  (tr7_engine_t tsc, int nargs, tr7_t *args, void *closure)
{
        int res = 0;
        for (int i = 0; i < nargs; i++)  {  
            int v = tr7_to_double (args[i]);
            res   = res | v;
        }
        tr7_t tres = tr7_from_int  (tsc, res);
        return tr7_C_return_single (tsc, tres);
}

// ----------------------------------------------------------------------------
// :section:    register
static const tr7_C_func_def_t   cexports[]  = {
    { "system",         system_cxp,             NULL,   TR7ARG_STRING,  1, 1 },
    { "process",        process_cxp,            NULL,   TR7ARG_STRING,  1, 1 },
    { "myadd",          myadd_cxp,              NULL,   TR7ARG_NUMBER,  2, 2 },
    { "xterm-width",    xterm_width,            NULL,   NULL,           0, 0},
    { "xterm-height",   xterm_height,           NULL,   NULL,           0, 0},
    { "nanosleep",      nanosleep_cxp,          NULL,   TR7ARG_NUMBER,  2, 2},
    { "file-touch-time",file_touch_time_cxp,    NULL,   TR7ARG_STRING,  1, 1},
    { "bit-shl",        bit_shl_cxp,            NULL,   TR7ARG_NUMBER,  2, 2},
    { "bit-asr",        bit_asr_cxp,            NULL,   TR7ARG_NUMBER,  2, 2},
    { "bit-and",        bit_and_cxp,            NULL,   TR7ARG_NUMBER,  0, 8},
    { "bit-or",         bit_or_cxp,             NULL,   TR7ARG_NUMBER,  0, 8},
};

static void init (tr7_engine_t tsc)
{
        tr7_register_C_functions(tsc, cexports, sizeof cexports / sizeof *cexports);
}


// ----------------------------------------------------------------------------
// :section:    commandline
void call_command (tr7_engine_t tsc, char *cmd)
{
        int max = 32;
        char line[32];
        int len;
        int i;
        if (cmd == NULL) {
            tr7_load_string (tsc, "(display \"#######  command missing: --cmd ...\n\")(exit)");
        } else {
            len = strlen (cmd);
            if (len > max) {
                tr7_load_string (tsc, "(display \"\n#######  command too long: -cmd ...\n\")(exit)");
            } else {
                line[0] = '(';
                for (i = 0; i < len; i++) line[i+1] = cmd[i];
                line[len+1] = ')';
                line[len+2] = 0;
                tr7_load_string (tsc, line);
            }
        }
}

void exec_expression (tr7_engine_t tsc, char *expr)
{
        size_t max = 64 * 1024;
        if (expr == NULL) {
            tr7_load_string (tsc, "(display \"#######  expression missing: -c ...\n\")(exit)");
        } else {
            if (strlen (expr) > max) {
                tr7_load_string (tsc, "(display \"\n#######  expression too long: -c ...\n\")(exit)");
            } else {
                tr7_load_string (tsc, expr);
            }
        }
}

void exec_pipe (tr7_engine_t tsc, char *expr)
{
        tr7_load_string (tsc, "(display \"\n#######  ROADWORK\nexec_pipe.\n\n\")(exit)");
}

// ============================================================================
// :chapter: MAIN
int main (int argc, char **argv)
{
        tr7_engine_t    tsc;

        // -------  no args: print short help message
        if (argc == 1) {
            print_helpmess();
            return 0;
        }

        // -------  -h: print short help message
        if ((argc == 2) && ((strcmp( argv[1], "-h") == 0) 
                         || (strcmp (argv[1], "--help") == 0))) {
            print_helpmess();
            return 0;
        }

        // -------  -v: print version info
        if ((argc == 2) && (strcmp( argv[1], "-v") == 0)) {
            printf (CONF_VERSIONSTR"\n");
            return 0;
        }

        // -------  start tsc
        setlocale (LC_ALL,      "");
        setlocale (LC_NUMERIC,  "C");
        tsc = tr7_engine_create( NULL );
        if (!tsc) {
            fprintf( stderr, "\n\n############  Error: tsc engine init failed.\n\n" );
            return 1;
        }
        import_buildin_libs    (tsc);
        tr7_set_standard_ports (tsc);
        tr7_set_argv   (argv);
        tr7_set_string (tsc, Tr7_StrID_Path,            CONF_PATH);
        tr7_set_string (tsc, Tr7_StrID_Library_Path,    CONF_LIBPATH);
        tr7_set_string (tsc, Tr7_StrID_Include_Path,    CONF_INCPATH);
        tr7_set_string (tsc, Tr7_StrID_Extension_Path,  CONF_EXTPATH);
        tr7_set_string (tsc, Tr7_StrID_Prompt,          CONF_PROMPT);

        init (tsc);

        /* tr7_display_string (tsc, CONF_VERSIONSTR);       // from here on printf disappears to happen */

        // load all scm files
        for (  argv++;                                                          // skip commandname
               ((*argv != NULL)                                                 // load all arg-files
                 && (strcmp (*argv, CONF_CMD_C_REPL)   != 0)                    // but not - + --cmd -c --
                 && (strcmp (*argv, CONF_CMD_SCM_REPL) != 0)
                 && (strcmp (*argv, CONF_CMD_FUN)      != 0)
                 && (strcmp (*argv, CONF_CMD_EXPR)     != 0)
                 && (strcmp (*argv, CONF_CMD_PIPE)     != 0));
               argv++) {
            tr7_load_file (tsc, NULL,  *argv);
        }

        // process last command line argument
        if ((*argv != NULL) && (strcmp (*argv, "-")) == 0)      tr7_load_file   (tsc, stdin, "<stdin>");
        if ((*argv != NULL) && (strcmp (*argv, "+")) == 0)      tr7_load_string (tsc, "(repl)");
        if ((*argv != NULL) && (strcmp (*argv,"--cmd")) == 0)   { argv++; call_command (tsc, *argv); }
        if ((*argv != NULL) && (strcmp (*argv,"-c")) == 0)      { argv++; exec_expression (tsc, *argv); }
        if ((*argv != NULL) && (strcmp (*argv,"--")) == 0)      { argv++; exec_pipe (tsc, *argv); }
} // end main


// ============================================================================
// :file: END sts-q 2023-Sep

